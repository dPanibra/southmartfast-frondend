export class ProductoPedido {
  public id: number;
  public created_at: string;
  public updated_at: string;
  public cantidad: number;
  public total: number;
  public producto_id: number;
  public pedido_id: number;
  public opcion_id: number;
  constructor(
    id: number,
    created_at: string,
    updated_at: string,
    cantidad: number,
    total: number,
    producto_id: number,
    pedido_id: number,
    opcion_id: number
  ) {
    this.id = id;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.cantidad = cantidad;
    this.total = total;
    this.producto_id = producto_id;
    this.pedido_id = pedido_id;
    this.opcion_id = opcion_id;
  }
}

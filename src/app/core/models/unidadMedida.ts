import { Producto } from './producto.model';

export class UnidadMedida {
  public id: number;
  public created_at: string;
  public updated_at: string;
  public nombre: string;
  public singular: string;
  public plural: string;
  public activo: number;
  public productos: Producto[];

  constructor(
    id: number,
    created_at: string,
    updated_at: string,
    nombre: string,
    singular: string,
    plural: string,
    activo: number,
    productos: Producto[]
  ) {
    this.id = id;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.nombre = nombre;
    this.singular = singular;
    this.plural = plural;
    this.activo = activo;
    this.productos = productos;
  }
}

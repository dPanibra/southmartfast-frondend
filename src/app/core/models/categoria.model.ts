import { Producto } from './producto.model';

export class Categoria {
  public id: number;
  public created_at: string;
  public updated_at: string;
  public nombre: string;
  public url_img: string;
  public activo: number;
  public n_orden: number;
  public productos: Producto[];

  constructor(
    id: number,
    created_at: string,
    updated_at: string,
    nombre: string,
    url_img: string,
    activo: number,
    n_orden: number,
    productos: Producto[]
  ) {
    this.id = id;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.nombre = nombre;
    this.url_img = url_img;
    this.activo = activo;
    this.n_orden = n_orden;
    this.productos = productos;
  }
}

export class Persona {
  public id: number;
  public created_at: string;
  public updated_at: string;
  public nombre: string;
  public email: string;
  public telefono: string;
  public direccion: string;
  public referencia: string;
  public lat: number;
  public lon: number;
  public distrito: string;

  constructor(
    id: number,
    created_at: string,
    updated_at: string,
    nombre: string,
    email: string,
    telefono: string,
    direccion: string,
    referencia: string,
    lat: number,
    lon: number,
    distrito: string
  ) {
    this.id = id;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.nombre = nombre;
    this.email = email;
    this.telefono = telefono;
    this.direccion = direccion;
    this.referencia = referencia;
    this.lat = lat;
    this.lon = lon;
    this.distrito = distrito;
  }
}

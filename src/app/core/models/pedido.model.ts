import { ProductoPedido } from './productoPedido.model';
export class Pedido {
  public id: number;
  public created_at: string;
  public updated_at: string;
  public observacion: string;
  public metodoPago: string;
  public pagoTotal: number;
  public entregaInmediata: number;
  public estado: string;
  public anulado_desc:string;
  public cl_nombre: string;
  public cl_email: string;
  public cl_telefono: string;
  public cl_direccion: string;
  public cl_referencia: string;
  public cl_lat: number;
  public cl_lon: number;
  public cl_distrito: string;
  public persona_id: number;

  constructor(
    id: number,
    created_at: string,
    updated_at: string,
    observacion: string,
    metodoPago: string,
    pagoTotal: number,
    entregaInmediata: number,
    estado: string,
    anulado_desc:string,
    cl_nombre: string,
    cl_email: string,
    cl_telefono: string,
    cl_direccion: string,
    cl_referencia: string,
    cl_lat: number,
    cl_lon: number,
    cl_distrito: string,
    persona_id: number
  ) {
    this.id = id;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.observacion = observacion;
    this.metodoPago = metodoPago;
    this.pagoTotal = pagoTotal;
    this.entregaInmediata=entregaInmediata;
    this.estado=estado;
    this.anulado_desc=anulado_desc;
    this.cl_nombre=cl_nombre;
    this.cl_email=cl_email;
    this.cl_telefono=cl_telefono;
    this.cl_direccion=cl_direccion;
    this.cl_referencia=cl_referencia;
    this.cl_lat=cl_lat;
    this.cl_lon=cl_lon;
    this.cl_distrito=cl_distrito;
    this.persona_id = persona_id;
  }
}

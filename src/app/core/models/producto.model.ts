export class Producto {
  public id: number;
  public created_at: string;
  public updated_at: string;
  public nombre: string;
  public codigo: string;
  public precio: number;
  public costo: number;
  public cantidad_venta: number;
  public stock: number;
  public activo: number;
  public descripcion_corta: string;
  public descripcion_larga: string;
  public images_url: string;
  public categoria_id: number;
  public unidad_medida_id: number;
  public descuento: number;
  public descuento_input: number;
  public descuento_porc: number;
  public descuento_act: number;
  public stockmin: number;
  public stockmin_input: number;
  public stockmin_porc: number;
  public stockcrit: number;
  public stockcrit_input: number;
  public stockcrit_porc: number;

  constructor(
    id: number,
    created_at: string,
    updated_at: string,
    nombre: string,
    codigo: string,
    precio: number,
    costo: number,
    cantidad_venta: number,
    stock: number,
    activo: number,
    descripcion_corta: string,
    descripcion_larga: string,
    images_url: string,
    categoria_id: number,
    unidad_medida_id: number,
    descuento: number,
    descuento_input: number,
    descuento_porc: number,
    descuento_act: number,
    stockmin: number,
    stockmin_input: number,
    stockmin_porc: number,
    stockcrit: number,
    stockcrit_input: number,
    stockcrit_porc: number
  ) {
    this.id = id;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.nombre = nombre;
    this.codigo = codigo;
    this.precio = precio;
    this.costo = costo;
    this.cantidad_venta = cantidad_venta;
    this.stock = stock;
    this.activo = activo;
    this.descripcion_corta = descripcion_corta;
    this.descripcion_larga = descripcion_larga;
    this.images_url = images_url;
    this.categoria_id = categoria_id;
    this.unidad_medida_id = unidad_medida_id;
    this.descuento = descuento;
    this.descuento_input = descuento_input;
    this.descuento_porc = descuento_porc;
    this.descuento_act = descuento_act;
    this.stockmin = stockmin;
    this.stockmin_input = stockmin_input;
    this.stockmin_porc = stockmin_porc;
    this.stockcrit = stockcrit;
    this.stockcrit_input = stockcrit_input;
    this.stockcrit_porc = stockcrit_porc;
  }
}

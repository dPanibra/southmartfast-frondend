export interface ProductCard {
  id: number;
  nombre: string;
  descripcion: string;
  cantidad_venta: number;
  img: any;
  unidad: string;
  precio: any;
  cantidad: number;
  total: any;
  descuento:any;
  descuento_act:boolean;
  descuento_input:any;
  descuento_porc:boolean;
  stock:number;
}
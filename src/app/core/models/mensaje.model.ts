export class Mensaje {
  public id: number;
  public created_at: string;
  public updated_at: string;
  public mensaje: string;
  public telefono: string;
  public forAdmin: number;
  public persona_id: number;
  public pedido_id: number;
  constructor(
    id: number,
    created_at: string,
    updated_at: string,
    mensaje: string,
    telefono: string,
    forAdmin: number,
    persona_id: number,
    pedido_id: number
  ) {
    this.id = id;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.mensaje = mensaje;
    this.telefono = telefono;
    this.forAdmin = forAdmin;
    this.persona_id = persona_id;
    this.pedido_id = pedido_id;
  }
}

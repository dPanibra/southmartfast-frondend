export class Config {
  public id: number;
  public nombreEmpresa: string;
  public textoCabecera: string;
  public correoAvisos: string;
  public celSMS: string;
  public celPagos: string;

  constructor(
    id: number,
    nombreEmpresa: string,
    textoCabecera: string,
    correoAvisos: string,
    celSMS: string,
    celPagos: string
  ) {
    this.id = id;
    this.nombreEmpresa = nombreEmpresa;
    this.textoCabecera = textoCabecera;
    this.correoAvisos = correoAvisos;
    this.celSMS = celSMS;
    this.celPagos = celPagos;
  }
}

export class Imagen {
  public imagen: any;
  public nombre: string;
  public url: any;

  constructor(imagen: any, nombre: string, url: any) {
    this.imagen = imagen;
    this.nombre = nombre;
    this.url = url;
  }
}

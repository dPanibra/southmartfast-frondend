import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { Observable } from 'rxjs/internal/Observable';
@Injectable({
  providedIn: 'root',
})
export class ScrollService {
  public entregaTop: number = 0;
  public pagoTop: number = 0;
  private place = new Subject<string>();

  constructor(){
  }
  getPlace(): Observable<string> {
    return this.place.asObservable();
  }
  updatePlace() {
    let myTop = window.scrollY;
    if (myTop < this.entregaTop) {
      this.place.next('Compra');
    } else if (myTop > this.entregaTop && myTop < this.pagoTop) {
      this.place.next('Entrega');
    } else if (myTop > this.pagoTop) {
      this.place.next('Pago');
    }
  }
  
}

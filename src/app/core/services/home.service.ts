import { EventEmitter, Injectable, Output } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { Observable } from 'rxjs/internal/Observable';
@Injectable({
  providedIn: 'root',
})
export class HomeService {
  @Output() change: EventEmitter<string> = new EventEmitter();
  private isOpen = new Subject<boolean>();
  constructor() {}
  getOpen(): Observable<boolean> {
    return this.isOpen.asObservable();
  }
  updateOpen(open: boolean) {
    this.isOpen.next(open);
  }
  goTo(place:string) {
    this.change.emit(place);
  }
}

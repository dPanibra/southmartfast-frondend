import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductoPedido } from './../models/productoPedido.model';

import { ProductCard } from './../models/productCard.inteface';

@Injectable({
  providedIn: 'root',
})
export class ProductoPedidoService {

  public productosCard: ProductCard[] = [];
  
  constructor(private http: HttpClient) {}

  

  // Obtener, Registrar, Actualizar y Eliminar ProductosPedidos de la BD

  getProductoPedidos(): Observable<any> {
    return this.http.get<Array<ProductoPedido>>('/productospedidos');
  }
  registrarProductoPedido(nuevoProductoPedido: any): Observable<any> {
    return this.http.post<any>('/productospedidos', nuevoProductoPedido);
  }
  getProductoPedidoById(idProductoPedido: number): Observable<ProductoPedido> {
    return this.http.get<any>(`/productospedidos/${idProductoPedido}`);
  }
  editProductoPedido(ProductoPedido: ProductoPedido): Observable<any> {
    return this.http.put<any>('/productospedidos', ProductoPedido);
  }
  deleteProductoPedido(idProductoPedido: number): Observable<any> {
    return this.http.delete<any>(`/productospedidos/${idProductoPedido}`);
  }
}

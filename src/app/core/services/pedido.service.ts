import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Pedido } from './../models/pedido.model';
import { ProductoPedidoService } from '@services/productoPedido.service';
export interface Delivery {
  distrito: string;
  precio: number;
}
@Injectable({
  providedIn: 'root',
})
export class PedidoService {
  public pedidoActual: Pedido = new Pedido(
    0,
    '',
    '',
    '',
    '',
    0,
    0,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    0,
    0,
    '',
    0
  );
  private totalPedido = new BehaviorSubject(Number(0).toFixed(2));
  private delivery = new BehaviorSubject({ name: '', precio: 0 });

  constructor(
    private http: HttpClient,
    private _sProductoPedido: ProductoPedidoService
  ) {}

  // Para Obtener y actualizar el precio Total del Pedido
  getTotalPedido(): Observable<any> {
    return this.totalPedido.asObservable();
  }
  updateTotalPedido() {
    this.getDelivery().subscribe((delivery) => {
      let total = 0;
      this._sProductoPedido.productosCard.forEach((pcard) => {
        total += +pcard.total;
      });
      let stringTotal = Number(total + +delivery.precio).toFixed(2);
      this.pedidoActual.pagoTotal = total + +delivery.precio;
      this.totalPedido.next(stringTotal);
    });
  }

  // Para Obtener y actualizar el Delivery
  getDelivery(): Observable<any> {
    return this.delivery.asObservable();
  }
  updateDelivery(delivery: any) {
    const delyUndefined = { name: '', precio: 0 };
    if (delivery == undefined) {
      this.delivery.next(delyUndefined);
    } else {
      this.delivery.next(delivery);
    }
  }

  // Obtener, Registrar, Actualizar y Eliminar Pedidos de la BD
  getPedidos(): Observable<any> {
    return this.http.get<Array<Pedido>>('/pedidos');
  }
  getPedidosFechas(
    tipo: string,
    fechIni?: string,
    fechFin?: string
  ): Observable<any> {
    return this.http.post<any>('/pedidosFechas', { tipo, fechIni, fechFin });
  }
  registrarPedido(nuevaPedido: Pedido): Observable<any> {
    return this.http.post<any>('/pedidos', nuevaPedido);
  }
  getPedidoById(idPedido: number): Observable<Pedido> {
    return this.http.get<any>(`/pedidos/${idPedido}`);
  }
  editPedido(pedido: Pedido): Observable<any> {
    return this.http.put<any>(`/pedidos/${pedido.id}`, pedido);
  }
  deletePedido(idPedido: number): Observable<any> {
    return this.http.delete<any>(`/pedidos/${idPedido}`);
  }
}

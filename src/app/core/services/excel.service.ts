import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { saveAs } from 'file-saver';
@Injectable({
  providedIn: 'root',
})
export class ExcelService {
  constructor(private http: HttpClient) {}

  getCategorias(): Observable<any> {
    return this.http.get<any>('/excel');
  }
  pedirExcel(pedidos): Observable<any> {
    return this.http.post('/excel', { pedidos:pedidos }, { responseType: 'blob' });
  }
  downLoadFile(data: any) {
    saveAs(data, 'pedidos.xlsx', { type: 'application/octet-stream' });
  }
}

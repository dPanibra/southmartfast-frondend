import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { Mensaje } from './../models/Mensaje.model';
@Injectable({
  providedIn: 'root',
})
export class MensajeService {

  constructor(private http: HttpClient) {}

  getMensajes(): Observable<any> {
    return this.http.get<Array<Mensaje>>('/mensajes');
  }
  registrarMensaje(nuevoMensaje: Mensaje): Observable<any> {
    return this.http.post<any>('/mensajes', nuevoMensaje);
  }
}

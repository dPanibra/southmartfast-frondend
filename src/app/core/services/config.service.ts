import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { Config } from './../models/config.model';
@Injectable({
  providedIn: 'root',
})
export class ConfigService {

  private generalConfig = new Subject<Config>();

  constructor(private http: HttpClient) {}

  // OBSERVABLES SERVICES
  getGeneralConfig(): Observable<Config> {
    return this.generalConfig.asObservable();
  }
  updategeneralConfig(generalConfig: Config) {
    this.generalConfig.next(generalConfig);
  }

  // HTTP SERVICES
  getConfig(): Observable<Config> {
    return this.http.get<Config>(`/configs/1`).pipe(
      tap((data) => this.updategeneralConfig(data))
    );
  }
  editConfig(Config: Config): Observable<any> {
    return this.http.put<any>(`/configs/1`, Config).pipe(
      tap((data) => this.updategeneralConfig(data.data))
    );
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private isLoged = new BehaviorSubject(false);

  constructor(private http: HttpClient,private router: Router) {}
  getisLoged(): Observable<any> {

    if(localStorage.getItem('QWxkZWxpTG9nZWRBZG1pbg==')=='true'){
      this.updateisLoged(true);
      return this.isLoged.asObservable();
    }else{
      this.updateisLoged(false);
      return this.isLoged.asObservable();
    }
  }
  updateisLoged(isLoged: any) {
    localStorage.setItem("QWxkZWxpTG9nZWRBZG1pbg==", isLoged);
    this.isLoged.next(isLoged);
  }
  logOut(){
    localStorage.clear();
    this.updateisLoged(false);
    this.router.navigateByUrl('/');

  }


  
}

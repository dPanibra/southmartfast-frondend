import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Producto } from './../models/producto.model';
@Injectable({
  providedIn: 'root',
})
export class ProductoService {


  constructor(private http: HttpClient) {}

//   getProductoActual(): Observable<Producto> {
//     return this.ProductoActual.asObservable();
//   }
//   updateProductoActual(Producto: Producto) {
//     this.ProductoActual.next(Producto);
//   }

  getProductos(): Observable<any> {
    return this.http.get<Array<Producto>>('/productos');
  }
  registrarProducto(nuevoProducto: Producto): Observable<any> {
    return this.http.post<any>('/productos', nuevoProducto);
  }
  getProductoById(idProducto: number): Observable<any> {
    return this.http.get<any>(`/productos/${idProducto}`);
  }
  editProducto(producto: Producto): Observable<any> {
    return this.http.put<any>(`/productos/${producto.id}`, producto);
  }
  deleteProducto(idProducto: number): Observable<any> {
    return this.http.delete<any>(`/productos/${idProducto}`);
  }
}

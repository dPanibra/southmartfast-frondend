import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Categoria } from './../models/categoria.model';
@Injectable({
  providedIn: 'root',
})
export class CategoriaService {
  constructor(private http: HttpClient) {}

  getCategorias(): Observable<any> {
    return this.http.get<Array<Categoria>>('/categorias');
  }
  getCategoriasWithProducts(): Observable<any> {
    return this.http.get<Array<Categoria>>('/categorias-products');
  }
  registrarCategoria(nuevaCategoria: Categoria): Observable<any> {
    return this.http.post<any>('/categorias', nuevaCategoria);
  }
  getCategoriaById(idCategoria: number): Observable<Categoria> {
    return this.http.get<any>(`/categorias/${idCategoria}`);
  }
  editCategoria(categoria: Categoria): Observable<any> {
    return this.http.put<any>(`/categorias/${categoria.id}`, categoria);
  }
  deleteCategoria(idCategoria: number): Observable<any> {
    return this.http.delete<any>(`/categorias/${idCategoria}`);
  }
}

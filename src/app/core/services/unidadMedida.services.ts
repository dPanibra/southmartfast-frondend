import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { UnidadMedida } from './../models/unidadMedida';
@Injectable({
  providedIn: 'root',
})
export class UnidadMedidaService {
  constructor(private http: HttpClient) {}

  getUnidadMedidas(): Observable<any> {
    return this.http.get<Array<UnidadMedida>>('/unidaddemedidas');
  }
  getUnidadMedidasWithProducts(): Observable<any> {
    return this.http.get<Array<UnidadMedida>>('/unidaddemedidas-products');
  }
  registrarUnidadMedida(nuevaUnidadMedida: UnidadMedida): Observable<any> {
    return this.http.post<any>('/unidaddemedidas', nuevaUnidadMedida);
  }
  getUnidadMedidaById(idUnidadMedida: number): Observable<UnidadMedida> {
    return this.http.get<any>(`/unidaddemedidas/${idUnidadMedida}`);
  }
  editUnidadMedida(UnidadMedida: UnidadMedida): Observable<any> {
    return this.http.put<any>(`/unidaddemedidas/${UnidadMedida.id}`, UnidadMedida);
  }
  deleteUnidadMedida(idUnidadMedida: number): Observable<any> {
    return this.http.delete<any>(`/unidaddemedidas/${idUnidadMedida}`);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { Imagen } from './../models/imagen.model';
@Injectable({
  providedIn: 'root',
})
export class ImagenService {

  constructor(private http: HttpClient) {}

  getImagens(): Observable<any> {
    return this.http.get<any>('/imagens');
  }
  registrarImagen(nuevaImagen: any): Observable<any> {
    return this.http.post<any>('/imagenes', nuevaImagen);
  }
}
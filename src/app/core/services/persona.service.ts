import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Persona } from './../models/persona.model';
@Injectable({
  providedIn: 'root',
})
export class PersonaService {
  public newPerson: Persona;
  public personValidator:boolean=false;

  private personaActual = new Subject<Persona>();

  constructor(private http: HttpClient) {}

  getPersonaActual(): Observable<Persona> {
    return this.personaActual.asObservable();
  }
  updatePersonaActual(persona: Persona) {
    this.personaActual.next(persona);
  }

  getPersonas(): Observable<any> {
    return this.http.get<Array<Persona>>('/personas');
  }
  registrarPersona(nuevaPersona: Persona): Observable<any> {
    return this.http.post<any>('/personas', nuevaPersona);
  }
  getPersonaById(idPersona: number): Observable<Persona> {
    return this.http.get<any>(`/personas/${idPersona}`);
  }
  editPersona(Persona: Persona): Observable<any> {
    return this.http.put<any>('/personas', Persona);
  }
  deletePersona(idPersona: number): Observable<any> {
    return this.http.delete<any>(`/personas/${idPersona}`);
  }
}

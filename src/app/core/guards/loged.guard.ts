import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './../services/login.service';

@Injectable({
  providedIn: 'root',
})
export class LogedGuard implements CanActivate {
  constructor(private router: Router, private _sLoginService: LoginService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (localStorage.getItem('QWxkZWxpTG9nZWRBZG1pbg==')=='true') {
      console.log(localStorage.getItem('QWxkZWxpTG9nZWRBZG1pbg=='));
      return true;
      
    } else {
      console.log(localStorage.getItem('QWxkZWxpTG9nZWRBZG1pbg=='));
      this.router.navigate(['']);
      return false;
    }
  }
}

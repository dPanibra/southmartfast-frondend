import { Component, OnInit, Inject } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { NguCarouselConfig } from '@ngu/carousel';

import { Producto } from '@models/producto.model';
import { DomSanitizer } from '@angular/platform-browser';

export interface data {
  imagenes: string[];
  descripcion: string;
}
@Component({
  selector: 'app-image-dialog',
  templateUrl: './image-dialog.component.html',
  styleUrls: ['./image-dialog.component.scss'],
})
export class ImageDialogComponent implements OnInit {
  carouselBanner;
  images: any[] = [];
  justOne: boolean = true;
  constructor(
    public dialogRef: MatDialogRef<ImageDialogComponent>,
    public deleteElement: MatDialogRef<ImageDialogComponent>,
    private sanitization: DomSanitizer,
    @Inject(MAT_DIALOG_DATA) public data: data
  ) {
    data.imagenes.forEach((img) => {
      this.images.push(this.sanitization.bypassSecurityTrustUrl(img));
    });
    this.justOne = data.imagenes.length > 1 ? false : true;
    console.log(this.images);
  }

  ngOnInit(): void {}
}

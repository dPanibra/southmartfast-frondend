import { Component, OnInit } from '@angular/core';
import { LoginService } from '@services/login.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit {
  constructor(private _sLogin: LoginService,) {}

  ngOnInit(): void {}
  salir() {
    this._sLogin.logOut();
  }
}

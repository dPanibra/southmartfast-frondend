import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkWhatsappComponent } from './link-whatsapp.component';

describe('LinkWhatsappComponent', () => {
  let component: LinkWhatsappComponent;
  let fixture: ComponentFixture<LinkWhatsappComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkWhatsappComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkWhatsappComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

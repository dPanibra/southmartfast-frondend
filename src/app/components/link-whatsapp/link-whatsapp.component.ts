import { Component, OnInit } from '@angular/core';
import { ConfigService } from '@services/config.service';
@Component({
  selector: 'app-link-whatsapp',
  templateUrl: './link-whatsapp.component.html',
  styleUrls: ['./link-whatsapp.component.scss'],
})
export class LinkWhatsappComponent implements OnInit {
  linkWsp:string;
  constructor(private _sConfig: ConfigService) {}

  ngOnInit(): void {
    this._sConfig.getGeneralConfig().subscribe((generalConfig) => {
      let nombreEmpresa=generalConfig.nombreEmpresa.split(' ').join('%20');      
      this.linkWsp=`https://api.whatsapp.com/send?phone=51${generalConfig.celSMS}&text=Hola%20${nombreEmpresa}`
    });
  }
}

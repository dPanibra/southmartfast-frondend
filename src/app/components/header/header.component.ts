import { Component, OnInit } from '@angular/core';
import { ConfigService } from '@services/config.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  textoCabecera: string;

  constructor(private _sConfig: ConfigService) {}

  ngOnInit(): void {
    this._sConfig.getGeneralConfig().subscribe((generalConfig) => {
      this.textoCabecera = generalConfig.textoCabecera;
    });
  }
}

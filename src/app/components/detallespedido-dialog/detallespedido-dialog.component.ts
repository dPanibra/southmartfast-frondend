import { getLocaleFirstDayOfWeek } from '@angular/common';
import { Component, OnInit, Inject } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Pedido } from '@models/pedido.model';
import { ConfigService } from '@services/config.service';
@Component({
  selector: 'app-detallespedido-dialog',
  templateUrl: './detallespedido-dialog.component.html',
  styleUrls: ['./detallespedido-dialog.component.scss'],
})
export class DetallespedidoDialogComponent implements OnInit {
  deliveryDistrito: any = 0;
  wspCliente = '';
  constructor(
    public dialogRef: MatDialogRef<DetallespedidoDialogComponent>,
    public deleteElement: MatDialogRef<DetallespedidoDialogComponent>,
    private _sConfig: ConfigService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    let totalProducts = 0;
    this.data.productos_pedidos.forEach((product) => {
      totalProducts += product.total;
    });
    this.deliveryDistrito = '5.00';
    console.log('gealesz');

    this.wspCliente = `https://api.whatsapp.com/send?phone=51${this.data.cl_telefono}&text=Buenas%20tardes%20${this.data.cl_nombre},%20delivery%20de%20South%20Mart`;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallespedidoDialogComponent } from './detallespedido-dialog.component';

describe('DetallespedidoDialogComponent', () => {
  let component: DetallespedidoDialogComponent;
  let fixture: ComponentFixture<DetallespedidoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallespedidoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallespedidoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

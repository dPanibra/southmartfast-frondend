import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { MaterialModule } from './material.module';

import { DeleteDialogComponent } from './components/delete-dialog/delete-dialog.component';
import { ImageDialogComponent } from './components/image-dialog/image-dialog.component';
import { DetallespedidoDialogComponent } from './components/detallespedido-dialog/detallespedido-dialog.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    NavBarComponent,
    DeleteDialogComponent,
    ImageDialogComponent,
    DetallespedidoDialogComponent,
  ],
  imports: [CommonModule, RouterModule, MaterialModule],
  exports: [
    HeaderComponent,
    FooterComponent,
    NavBarComponent,
    DeleteDialogComponent,
    ImageDialogComponent,
    DetallespedidoDialogComponent,
  ],
  entryComponents: [
    DeleteDialogComponent,
    ImageDialogComponent,
    DetallespedidoDialogComponent,
  ],
})
export class ComponentsModule {}

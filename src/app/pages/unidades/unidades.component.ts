import { Component, OnInit } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { UnidadMedidaService } from '@services/unidadMedida.services';
import { UnidadMedida } from '@models/unidadMedida';
import { Producto } from '@models/producto.model';
import { ProductoService } from '@services/producto.service';
export interface UnidadMedidaTabla {
  id: number;
  nombre: string;
  singular: string;
  plural: string;
  activado: boolean;
  toggleDisable: boolean;
  productos: Producto[];
}
@Component({
  selector: 'app-unidades',
  templateUrl: './unidades.component.html',
  styleUrls: ['./unidades.component.scss'],
})
export class UnidadesComponent implements OnInit {
  loading: boolean = true;
  listUnidades: UnidadMedida[] = [];
  listUnidadesTabla: UnidadMedidaTabla[] = [];
  listProductos: Producto[] = [];
  constructor(
    private _sUnidadMedida: UnidadMedidaService,
    private _sProducto: ProductoService,
    private _snackBar: MatSnackBar
  ) {}
  ngOnInit(): void {
    this._sUnidadMedida.getUnidadMedidasWithProducts().subscribe(
      (data: UnidadMedida[]) => {
        console.log(data);
        this.listUnidades = data;
        this.listUnidades.forEach((elem) => {
          elem.productos.forEach((productin: Producto) => {
            this.listProductos.push(productin);
          });
          let newCategoriaTabla: UnidadMedidaTabla = {
            id: elem.id,
            nombre: elem.nombre,
            singular: elem.singular,
            plural: elem.plural,
            activado: elem.activo == 1 ? true : false,
            toggleDisable: false,
            productos: elem.productos,
          };
          this.listUnidadesTabla.push(newCategoriaTabla);
        });
        this.loading = false;
        console.log(this.listProductos);
      },
      (error) => {
        console.log(error);
      }
    );
  }
  toggle(uni: UnidadMedidaTabla) {
    uni.toggleDisable = true;
    let unidadEdit: UnidadMedida = this.listUnidades.find(
      (element) => element.id == uni.id
    );
    unidadEdit.activo = uni.activado ? 1 : 0;
    this._sUnidadMedida.editUnidadMedida(unidadEdit).subscribe(
      (data) => {
        if (data.message == 'success') {
          uni.toggleDisable = false;
          this.openSnackBar(
            unidadEdit.nombre,
            unidadEdit.activo,
            'Unidad de Medida'
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }



  drop(event: CdkDragDrop<string[]>, uniMedida: UnidadMedidaTabla) {
    const nombre = event.item.element.nativeElement.innerText;
    let productoEdit: Producto = this.listProductos.find(
      (product: Producto) => {
        return product.nombre === nombre;
      }
    );
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      productoEdit.unidad_medida_id = uniMedida.id;
      this._sProducto.editProducto(productoEdit).subscribe(
        (data) => {
          this.openSnackBar(
            productoEdit.nombre,
            productoEdit.activo,
            'Producto'
          );
        },
        (error) => console.log(error)
      );
    }
  }
  openSnackBar(nombre: string, activo: number, tipo: string) {
    const activado = activo == 1 ? 'Activada' : 'Desactivada';
    let mensaje: string =
      tipo == 'Producto'
        ? `Su Producto ${nombre} ha sido actualizado exitosamente`
        : `Su Unidad de Medida ${nombre} ha sido ${activado} exitosamente`;
    let config = new MatSnackBarConfig();
    config.duration = 2500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(mensaje, 'x', config);
  }
}

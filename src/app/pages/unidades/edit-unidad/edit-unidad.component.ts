import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { UnidadMedida } from '@models/unidadMedida';
import { UnidadMedidaService } from '@services/unidadMedida.services';

import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from './../../../components/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-edit-unidad',
  templateUrl: './edit-unidad.component.html',
  styleUrls: ['./edit-unidad.component.scss'],
})
export class EditUnidadComponent implements OnInit {
  myForm: FormGroup;
  newUnidadMedida: UnidadMedida = new UnidadMedida(
    0,
    '',
    '',
    '',
    '',
    '',
    1,
    []
  );
  btnState = {
    normal: true,
    disableBtn: false,
    loader: false,
    success: false,
  };
  load: boolean = true;
  edit: boolean = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private _sUnidadMedida: UnidadMedidaService,
    public dialog: MatDialog
  ) {
    this.route.params.subscribe((params) => {
      this.getUnidadMedida(params.id);
    });
  }

  ngOnInit(): void {
    this.myForm = this.formBuilder.group({
      nombre: ['', [Validators.required, this.noWhitespaceValidator]],
      singular: ['', [Validators.required, this.noWhitespaceValidator]],
      plural: ['', [Validators.required, this.noWhitespaceValidator]],
    });
  }
  getUnidadMedida(id: number) {
    this._sUnidadMedida.getUnidadMedidaById(id).subscribe(
      (data: UnidadMedida) => {
        console.log(data);
        if (data) {
          this.newUnidadMedida = data;
          this.myForm.disable();
          this.load = false;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  editForm() {
    this.myForm.enable();
    this.edit = true;
  }

  onSubmit() {
    if (this.myForm.valid === true) {
      this.btnState.disableBtn = true;
      this.btnState.normal = false;
      this.btnState.loader = true;
      this.myForm.disable();
      this.updateUnidadMedida(this.newUnidadMedida);
    }
  }
  updateUnidadMedida(newUnidadMedida: UnidadMedida) {
    this._sUnidadMedida.editUnidadMedida(newUnidadMedida).subscribe(
      (data) => {
        if (data.message == 'success') {
          console.log(data);
          this.btnState.loader = false;
          this.btnState.success = true;
          this.openSnackBar();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  openSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `La Unidad de Medida ${this.newUnidadMedida.nombre} se ha actualizado exitosamente`,
      'x',
      config
    );
    setTimeout(() => {
      this.goBack();
    }, 1500);
  }

  goBack() {
    this.router.navigateByUrl('/unidadesdemedida');
  }

  openDeleteSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['delete-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `La Unidad de Medida ${this.newUnidadMedida.nombre} ha sido eliminada`,
      'x',
      config
    );
    setTimeout(() => {
      this.goBack();
    }, 1500);
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '400px',
      data: { type: 'Unidad de Medida', name: this.newUnidadMedida.nombre },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === true) {
        this.deleteUnidadMedida(this.newUnidadMedida.id);
      }
    });
  }

  deleteUnidadMedida(id: number) {
    this._sUnidadMedida.deleteUnidadMedida(id).subscribe(
      (data) => {
        if (data.message == 'success') {
          this.openDeleteSnackBar();
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }
  noWhitespaceValidator(control: FormControl) {
    if (control.value.toString().length == 0) {
      const isValid = true;
      return isValid;
    } else {
      const isWhitespace = (control.value || '').trim().length == 0;
      const isValid = !isWhitespace;
      return isValid ? null : { whitespace: true };
    }
  }
}

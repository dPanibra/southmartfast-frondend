import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnidadesRoutingModule } from './unidades-routing.module';
import { UnidadesComponent } from './unidades.component';
import { AddUnidadComponent } from './add-unidad/add-unidad.component';
import { EditUnidadComponent } from './edit-unidad/edit-unidad.component';
import { MaterialModule} from './../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [UnidadesComponent, AddUnidadComponent, EditUnidadComponent],
  imports: [
    CommonModule,
    UnidadesRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UnidadesModule { }

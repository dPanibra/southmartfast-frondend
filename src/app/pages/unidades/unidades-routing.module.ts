import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnidadesComponent} from './unidades.component'
import { AddUnidadComponent} from './add-unidad/add-unidad.component'
import { EditUnidadComponent} from './edit-unidad/edit-unidad.component'

const routes: Routes = [
  {path:'',component:UnidadesComponent},
  {path:'nuevaUnidad',component:AddUnidadComponent},
  {path:'detallesUnidad/:id',component:EditUnidadComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnidadesRoutingModule { }

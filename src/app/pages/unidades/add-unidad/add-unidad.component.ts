import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { UnidadMedida } from '@models/unidadMedida';
import { UnidadMedidaService } from '@services/unidadMedida.services';

import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
@Component({
  selector: 'app-add-unidad',
  templateUrl: './add-unidad.component.html',
  styleUrls: ['./add-unidad.component.scss'],
})
export class AddUnidadComponent implements OnInit {
  myForm: FormGroup;
  newUnidadMedida: UnidadMedida = new UnidadMedida(0, '','','','','',1,[]);
  btnState = {
    normal: true,
    disableBtn: false,
    loader: false,
    success: false,
  };
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private _sUnidadMedida: UnidadMedidaService
  ) {}

  ngOnInit(): void {
    this.myForm = this.formBuilder.group({
      nombre: ['', [Validators.required, this.noWhitespaceValidator]],
      singular: ['', [Validators.required, this.noWhitespaceValidator]],
      plural: ['', [Validators.required, this.noWhitespaceValidator]],
    });
  }
  onSubmit() {
    if (this.myForm.valid === true) {
      this.myForm.disable();
      this.registrarUnidadMedida(this.newUnidadMedida);
    }
  }
  registrarUnidadMedida(newUnidadMedida: UnidadMedida) {
    this.btnState.disableBtn = true;
    this.btnState.normal = false;
    this.btnState.loader = true;
    this._sUnidadMedida.registrarUnidadMedida(newUnidadMedida).subscribe(
      (data) => {
        if (data.message == 'success') {
          console.log(data);
          this.btnState.loader = false;
          this.btnState.success = true;
          this.openSnackBar();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  openSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `La Categoria ${this.newUnidadMedida.nombre} se ha registrado exitosamente`,
      'x',
      config
    );
    setTimeout(() => {
      this.goBack();
    }, 1500);
  }

  goBack() {
    this.router.navigateByUrl('/unidadesdemedida');
  }
  noWhitespaceValidator(control: FormControl) {
    if (control.value.toString().length == 0) {
      const isValid = true;
      return isValid;
    } else {
      const isWhitespace = (control.value || '').trim().length == 0;
      const isValid = !isWhitespace;
      return isValid ? null : { whitespace: true };
    }
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';

import { PedidoService } from '@services/pedido.service';
import { Pedido } from '@models/pedido.model';
import { ProductoService } from '@services/producto.service';
import { Producto } from '@models/producto.model';
import { ExcelService } from '@services/excel.service';

import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { DetallespedidoDialogComponent } from './../../components/detallespedido-dialog/detallespedido-dialog.component';

import * as _moment from 'moment';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { fromMatSort, sortRows } from './datasource-utils';
import { fromMatPaginator, paginateRows } from './datasource-utils';
const moment = _moment;

export interface btnOption {
  tipo: string;
  label: string;
  selected: boolean;
}
export interface pedidosState {
  load: boolean;
  nPendiente: number;
  nRealizado: number;
  nAnulado: number;
  label: string;
}
export interface pedidosTabla {
  id: number;
  fecha: string;
  cliente: string;
  formaPago: string;
  direccion: string;
  distrito: string;
  total: string;
  estado: string;
  observacion: string;
  pPedido: any[];
}

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss'],
})
export class PedidosComponent implements OnInit {
  isTablas: boolean = false;
  openProducts: boolean = true;

  panelOpenState: boolean = false;
  state: pedidosState = {
    load: false,
    nPendiente: 0,
    nRealizado: 0,
    nAnulado: 0,
    label: 'De la Semana',
  };
  pPendiente: Pedido[] = [];
  pRealizado: Pedido[] = [];
  pAnulado: Pedido[] = [];
  productos: Producto[] = [];
  pedidos: Pedido[] = [];
  excelPedidos: any[] = [];

  btnOptions: btnOption[] = [
    { tipo: 'toDay', label: 'Del Día', selected: false },
    { tipo: 'thisWeek', label: 'De la Semana', selected: true },
    { tipo: 'thisMonth', label: 'Del Mes', selected: false },
  ];

  // Enviroments for BTN BetweenDays

  betweenBtn = {
    label: 'Entre Días',
    state: 'empty',
    selected: false,
    tipo: 'betweenDay',
  };

  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });

  btnExcelLoading: boolean = false;

  // variables tabla
  private sort: MatSort;

  @ViewChild(MatSort, { static: false }) set matSort(ms: MatSort) {
    this.sort = ms;
    // this.setDataSourceAttributes();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedRows$: Observable<pedidosTabla[]>;
  totalRows$: Observable<number>;

  constructor(
    private _sPedido: PedidoService,
    private _sProductos: ProductoService,
    private _sExcel: ExcelService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getProductos();
  }

  getProductos() {
    this._sProductos.getProductos().subscribe((data: Producto[]) => {
      if (data) {
        this.productos = data;
        this.getPedidosBtnOption('thisWeek');
      }
    });
  }
  emptyLists() {
    this.pedidos = [];
    this.pPendiente = [];
    this.pRealizado = [];
    this.pAnulado = [];
    this.excelPedidos = [];
    // this.dataTablaPedidos = new MatTableDataSource([]);
  }

  fillLists(pedidos: any[]) {
    let pedidosTabla: pedidosTabla[] = [];
    this.pedidos = pedidos;
    pedidos.forEach((pedido) => {
      pedido.productos_pedidos.forEach((pPedido) => {
        this.agregarNombre(pPedido);
      });
      this.excelPedidos.push(pedido);
      console.log(pedido);

      switch (pedido.estado) {
        case 'Pendiente':
          this.pPendiente.push(pedido);
          break;
        case 'Realizado':
          this.pRealizado.push(pedido);
          break;
        case 'Anulado':
          this.pAnulado.push(pedido);
          break;
      }
      pedidosTabla.push({
        id: pedido.id,
        fecha: pedido.created_at,
        cliente: pedido.cl_nombre,
        formaPago: pedido.metodoPago,
        direccion: pedido.cl_direccion,
        distrito: pedido.cl_distrito,
        total: Number(pedido.pagoTotal).toFixed(2),
        estado: pedido.estado,
        observacion:
          pedido.observacion == null ? 'Sin Observaciones' : pedido.observacion,
        pPedido: pedido.productos_pedidos,
      });
    });
    this.state.load = true;
    this.actualizarContadores();
    this.matTable(pedidosTabla);
  }
  matTable(listPedidos: pedidosTabla[]) {
    // this.sort = ms;

    const sortEvents$: Observable<Sort> = fromMatSort(this.sort);
    const pageEvents$: Observable<PageEvent> = fromMatPaginator(this.paginator);

    const rows$ = of(listPedidos);

    this.totalRows$ = rows$.pipe(map((rows) => rows.length));
    this.displayedRows$ = rows$.pipe(
      sortRows(sortEvents$),
      paginateRows(pageEvents$)
    );
  }
  agregarNombre(pPedido) {
    Object.defineProperty(pPedido, 'nombre', {
      enumerable: false,
      configurable: false,
      writable: false,
      value:
        this.productos.find((product) => product.id == pPedido.producto_id)
          .nombre || '',
    });
  }
  drop(event: CdkDragDrop<string[]>, nuevoEstado: string) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      const pedidoId = +event.item.element.nativeElement.id;
      const pedido: Pedido = this.pedidos.find((pedid) => pedid.id == pedidoId);
      pedido.estado = nuevoEstado;
      // console.log(pedido.estado);


      this._sPedido.editPedido(pedido).subscribe((data) => {
        if (data.message == 'success') {
          this.openSnackBar(pedido.cl_nombre, nuevoEstado);
        }
      });
    }
    this.actualizarContadores();
  }

  actualizarContadores() {
    this.state.nPendiente = this.pPendiente.length;
    this.state.nRealizado = this.pRealizado.length;
    this.state.nAnulado = this.pAnulado.length;
  }

  openDetails(pedido): void {
    const dialogRef = this.dialog.open(DetallespedidoDialogComponent, {
      width: '900px',
      data: pedido,
    });
  }
  openSnackBar(nombreCliente: string, estado: string) {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `El pedido de ${nombreCliente} ha sido exitosamente ${estado}!`,
      'x',
      config
    );
  }

  btnOption(tipo: string) {
    this.betweenBtn.selected = false;
    const option = this.btnOptions.find((option) => option.tipo == tipo);
    const isSelected = option.selected;
    if (!isSelected) {
      this.btnOptions.forEach((option: btnOption) => {
        option.selected = option.tipo == tipo ? true : false;
      });
      this.state.load = false;
      this.state.label = option.label;
      this.getPedidosBtnOption(tipo);
    }
  }

  getPedidosBtnOption(tipo: string) {
    this.emptyLists();
    this._sPedido.getPedidosFechas(tipo).subscribe((data: Pedido[]) => {
      this.fillLists(data);
      this.state.load = true;
    });
  }

  changeDate() {
    const format2 = 'YYYY-MM-DD';
    const fechIni = moment(this.range.value.start).format(format2);
    const fechFin = moment(this.range.value.end).format(format2);

    this.betweenBtn.label = `Del ${fechIni} al ${
      fechFin == 'Invalid date' ? '' : fechFin
    }`;
    if (this.range.value.start != null && this.range.value.end != null) {
      this.betweenBtn.state = 'full';
    } else {
      this.betweenBtn.state = 'empty';
    }
  }
  getBetweenPedidos() {
    this.stateBetween();
    const format2 = 'YYYY-MM-DD';
    const fechIni = moment(this.range.value.start).format(format2);
    const fechFin = moment(this.range.value.end).format(format2);

    this._sPedido
      .getPedidosFechas(this.betweenBtn.tipo, fechIni, fechFin)
      .subscribe((data: Pedido[]) => {
        this.fillLists(data);
        this.state.load = true;
        this.betweenBtn.label = 'Entre Días';
        this.betweenBtn.state = 'empty';
      });
  }
  stateBetween() {
    this.btnOptions.forEach((option: btnOption) => {
      option.selected = false;
    });
    this.emptyLists();
    this.betweenBtn.selected = true;
    this.betweenBtn.state = 'loading';

    this.state.load = false;
    this.state.label = this.betweenBtn.label;
  }

  async downloadExcel() {
    if (this.pedidos.length > 0) {
      this.btnExcelLoading = true;
      await this.excelPedidos.forEach((pedido) => {
        pedido.productos_pedidos.forEach((pPedido) => {
          pPedido.opcion_id =
            this.productos.find((product) => product.id == pPedido.producto_id)
              .nombre || '';
        });
      });
      await console.log(this.excelPedidos);

      await this._sExcel.pedirExcel(this.excelPedidos).subscribe(
        (data) => {
          console.log('resp', data);
          this._sExcel.downLoadFile(data);
          this.btnExcelLoading = false;
        },
        (error) => {
          console.error(error);

          this.btnExcelLoading = false;
        }
      );
    } else {
      alert('No tiene Pedidos para descargar');
    }
  }
}

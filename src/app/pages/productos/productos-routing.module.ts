import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductosComponent } from './productos.component';
import { AddProductoComponent } from './add-producto/add-producto.component';
import { EditProductoComponent} from './edit-producto/edit-producto.component'
const routes: Routes = [
  { path: '', component: ProductosComponent },
  { path: 'nuevoProducto', component: AddProductoComponent },
  { path: 'detallesProducto/:id', component: EditProductoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductosRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductosRoutingModule } from './productos-routing.module';
import { ProductosComponent } from './productos.component';
import { AddProductoComponent } from './add-producto/add-producto.component';

import { MaterialModule} from './../../material.module';
import { EditProductoComponent } from './edit-producto/edit-producto.component'


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [ProductosComponent, AddProductoComponent, EditProductoComponent],
  imports: [
    CommonModule,
    ProductosRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ProductosModule { }

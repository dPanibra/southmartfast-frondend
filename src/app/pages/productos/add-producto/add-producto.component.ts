import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
} from '@angular/forms';

import { Producto } from '@models/producto.model';
import { ProductoService } from '@services/producto.service';
import { CategoriaService } from '@services/categoria.service';
import { UnidadMedidaService } from '@services/unidadMedida.services';
import { Categoria } from '@models/categoria.model';
import { UnidadMedida } from '@models/unidadMedida';
import { Imagen } from '@models/imagen.model';
import { ImagenService } from '@services/imagen.service';

import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { environment } from './../../../../environments/environment';
import { MatFormField } from '@angular/material/form-field';

@Component({
  selector: 'app-add-producto',
  templateUrl: './add-producto.component.html',
  styleUrls: ['./add-producto.component.scss'],
})
export class AddProductoComponent implements OnInit {
  @ViewChild('inputImg') inputImg: MatFormField;

  myForm: FormGroup;
  newProducto = new Producto(
    0,
    '',
    '',
    '',
    '',
    null,
    null,
    1,
    null,
    1,
    '',
    '',
    '',
    null,
    null,
    null,
    null,
    1,
    0,
    null,
    null,
    1,
    null,
    null,
    1
  );

  imgLocales: Imagen[] = [];
  urls: string[] = [];
  imgChange = { change: false, index: 0 };
  img: any = null;
  categorias: Categoria[] = [];
  unidadMedidas: UnidadMedida[] = [];
  btnState = {
    normal: true,
    disableBtn: false,
    loader: false,
    success: false,
  };
  inputIMGdisable: boolean = false;

  toggleDesc: boolean = true;
  toggleStockCrit: boolean = true;
  toggleStockMin: boolean = true;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private _sProducto: ProductoService,
    private _sCategoria: CategoriaService,
    private _sUnidadMedida: UnidadMedidaService,
    private _sImagen: ImagenService,
    private _snackBar: MatSnackBar
  ) {
    this.myForm = this.formBuilder.group({
      nombre: ['', [Validators.required, this.noWhitespaceValidator]],
      categoria: ['', [Validators.required]],
      unidad: ['', [Validators.required]],
      costo: ['', [Validators.required]],
      precio: ['', [Validators.required]],
      stock: ['', [Validators.required]],
      descripcion_corta: ['', [this.noWhitespaceValidator]],
      descripcion_larga: ['', [this.noWhitespaceValidator]],
      newInput: ['', [this.noWhitespaceValidator]],
      // descToggle: ['', [this.noWhitespaceValidator]],
      // descripcion_larga: ['', [this.noWhitespaceValidator]],
    });
    this.getCategorias();
    this.getUnidadMedidas();
  }

  ngOnInit(): void {}

  getCategorias() {
    this._sCategoria.getCategorias().subscribe(
      (data: Categoria[]) => {
        if (data) {
          this.categorias = data
            .filter((cate) => cate.activo === 1)
            .sort((a, b) => a.n_orden - b.n_orden);
        }
      },
      (error) => console.log(error)
    );
  }
  getUnidadMedidas() {
    this._sUnidadMedida.getUnidadMedidas().subscribe(
      (data: UnidadMedida[]) => {
        if (data) {
          this.unidadMedidas = data.filter((uni) => uni.activo === 1);
        }
      },
      (error) => console.log(error)
    );
  }

  onSubmit() {
    if (this.myForm.valid === true) {
      this.btnState.disableBtn = true;
      this.btnState.normal = false;
      this.btnState.loader = true;
      this.inputIMGdisable = true;
      this.myForm.disable();
      this.newProducto.costo = +Number(this.newProducto.costo).toFixed(2);
      this.newProducto.precio = +Number(this.newProducto.precio).toFixed(2);
      this.newProducto.descuento_porc = this.toggleDesc == true ? 1 : 0;
      this.newProducto.stockmin_porc = this.toggleStockMin == true ? 1 : 0;
      this.newProducto.stockcrit_porc = this.toggleStockCrit == true ? 1 : 0;
      this.newProducto.descuento_act = this.newProducto.descuento_input > 0 ? 1 : 0;
      console.log(this.newProducto);
      if (this.imgLocales.length == 0) {
        this.registrarProducto();
      } else {
        this.registraIMG();
      }
    }
  }

  registrarProducto() {
    this._sProducto.registrarProducto(this.newProducto).subscribe(
      (data) => {
        if (data.message == 'success') {
          console.log(data);
          this.btnState.loader = false;
          this.btnState.success = true;
          this.openSnackBar();
        } else if (data.error == 'Producto ya registrado') {
          this.btnState = {
            normal: true,
            disableBtn: false,
            loader: false,
            success: false,
          };
          this.myForm.enable();
          this.newProducto.nombre = '';
          this.urls = [];
          this.openErrorSnackBar(this.newProducto.nombre);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  // Agregar Imagen
  inputImgClick() {
    const nImg = 25;
    this.imgLocales.length <= nImg
      ? this.inputImg._elementRef.nativeElement.click()
      : '';
    console.log(this.urls.length);
  }
  changeImgClick(index: number) {
    this.imgChange = { change: true, index };
    this.inputImgClick();
  }

  readURL(event): void {
    if (event.target.files && event.target.files[0]) {
      if (this.imgChange.change) {
        let imgChange = this.imgLocales[this.imgChange.index];
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = (e) => (imgChange.url = reader.result);
        reader.readAsDataURL(file);
        imgChange.imagen = file;
        this.imgChange = { change: false, index: 0 };
      } else {
        let img: Imagen = { imagen: '', nombre: '', url: '' };
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = (e) => (img.url = reader.result);
        reader.readAsDataURL(file);
        img.imagen = file;
        img.nombre = `Imagen ${
          this.imgLocales.length > 0 ? this.imgLocales.length : 'Principal'
        }`;
        this.imgLocales.push(img);
      }
    }
  }

  registraIMG() {
    this._sImagen
      .registrarImagen(this.getForm(this.imgLocales[this.urls.length]))
      .subscribe(
        (data) => {
          let url: string = environment.baseURL + '/' + data.data;
          this.urls.push(url);
          console.log('Registrado:', url);
          if (this.urls.length == this.imgLocales.length) {
            const urlsString: string = this.urls.join(',');
            this.newProducto.images_url = urlsString;
            console.log('String url images:', this.newProducto.images_url);

            this.registrarProducto();
          } else {
            this.registraIMG();
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  getForm(img: Imagen): FormData {
    let formImg = new FormData();
    formImg.append('file', img.imagen);
    formImg.append('tipo', 'productos');
    return formImg;
  }

  // Descuento, Stock Min - Crit

  desc() {
    if (this.toggleDesc) {
      let porcent =
        this.newProducto.precio * (this.newProducto.descuento_input / 100);
      this.newProducto.descuento = +Number(porcent).toFixed(2);
    } else {
      this.newProducto.descuento = this.newProducto.descuento_input;
    }
  }
  stocks() {
    this.stockMin();
    this.stockCrit();
  }
  stockMin() {
    if (this.toggleStockMin) {
      let porcent =
        this.newProducto.stock * (this.newProducto.stockmin_input / 100);
      this.newProducto.stockmin = +Number(porcent).toFixed(2);
    } else {
      this.newProducto.stockmin = this.newProducto.stockmin_input;
    }
  }
  stockCrit() {
    if (this.toggleStockCrit) {
      let porcent =
        this.newProducto.stock * (this.newProducto.stockcrit_input / 100);
      this.newProducto.stockcrit = +Number(porcent).toFixed(2);
    } else {
      this.newProducto.stockcrit = this.newProducto.stockcrit_input;
    }
  }

  openSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `Su Producto ${this.newProducto.nombre} se ha registrado exitosamente`,
      'x',
      config
    );
    setTimeout(() => {
      this.goBack();
    }, 1500);
  }

  openErrorSnackBar(name) {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['delete-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(`El Producto ${name} ya esta registrado`, 'x', config);
  }

  goBack() {
    this.router.navigateByUrl('/productos');
  }
  noWhitespaceValidator(control: FormControl) {
    if (control.value.toString().length == 0) {
      const isValid = true;
      return isValid;
    } else {
      const isWhitespace = (control.value || '').trim().length == 0;
      const isValid = !isWhitespace;
      return isValid ? null : { whitespace: true };
    }
  }
}

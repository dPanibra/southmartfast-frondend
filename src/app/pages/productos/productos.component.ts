import { Component, OnInit, ViewChild } from '@angular/core';

import { Producto } from '@models/producto.model';
import { ProductoService } from '@services/producto.service';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { CategoriaService } from '@services/categoria.service';
import { Categoria } from '@models/categoria.model';

import { UnidadMedida } from '@models/unidadMedida';
import { UnidadMedidaService } from '@services/unidadMedida.services';

export interface ProductoTabla {
  id: number;
  nombre: string;
  categoria: string;
  unidad: string;
  descripcion: string;
  costo: string;
  precio: string;
  stock: number;
  activado: boolean;
  descuento: boolean;
  toggleDisable: boolean;
}

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss'],
})
export class ProductosComponent implements OnInit {
  listaProductos: Producto[] = [];

  load: boolean = true;
  dataSource: MatTableDataSource<ProductoTabla>;

  columnas: string[] = [
    'id',
    'nombre',
    'categoria',
    'unidad',
    'descripcion',
    'costo',
    'precio',
    'stock',
    'activado',
    'descuento',
  ];

  categorias: Categoria[] = [];
  unidadesMedidas: UnidadMedida[] = [];

  private paginator: MatPaginator;
  private sort: MatSort;

  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    if (this.paginator && this.sort) {
      this.applyFilter(event);
    }
  }

  constructor(
    private _sCategoria: CategoriaService,
    private _sUnidadMedida: UnidadMedidaService,
    private _sProducto: ProductoService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.getCategorias();
  }
  getCategorias() {
    this._sCategoria.getCategorias().subscribe(
      (data: Categoria[]) => {
        if (data) {
          this.categorias = data;
          this.getUnidadesMedidas();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  getUnidadesMedidas() {
    this._sUnidadMedida.getUnidadMedidas().subscribe(
      (data: UnidadMedida[]) => {
        if (data) {
          this.unidadesMedidas = data;
          this.getProductos();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  getProductos() {
    this._sProducto.getProductos().subscribe((data: Producto[]) => {
      this.listaProductos = data;
      let listaProductTabla: ProductoTabla[] = [];
      data.forEach((product) => {
        let newProductTabla: ProductoTabla = {
          id: product.id,
          nombre: product.nombre,
          categoria: this.categorias.find(
            (cate) => cate.id === product.categoria_id
          ).nombre,
          unidad: this.unidadesMedidas.find(
            (uni) => uni.id === product.unidad_medida_id
          ).nombre,
          descripcion: product.descripcion_corta,
          costo: Number(product.costo).toFixed(2),
          precio: Number(product.precio).toFixed(2),
          stock: product.stock,
          activado: product.activo == 1 ? true : false,
          descuento: product.descuento_act == 1 ? true : false,
          toggleDisable: false,
        };
        listaProductTabla.push(newProductTabla);
      });
      this.matTable(listaProductTabla);
    });
  }
  matTable(listClientes: Array<ProductoTabla>) {
    this.load = false;
    this.dataSource = new MatTableDataSource(listClientes);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  toggle(prod: ProductoTabla, activacion: boolean) {
    prod.toggleDisable = true;
    let productoEdit: Producto = this.listaProductos.find(
      (element) => element.id == prod.id
    );
    activacion
      ? (productoEdit.activo = prod.activado ? 1 : 0)
      : (productoEdit.descuento_act = prod.descuento ? 1 : 0);

    this._sProducto.editProducto(productoEdit).subscribe(
      (data) => {
        if (data.message == 'success') {
          prod.toggleDisable = false;
          activacion
            ? this.openSnackBar(productoEdit.nombre, productoEdit.activo, false)
            : this.openSnackBar(
                productoEdit.nombre,
                productoEdit.descuento,
                true
              );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  openSnackBar(nombre: string, activo: number, desc: boolean) {
    const activado = activo == 1 ? 'Activado' : 'Desactivado';
    const descText = desc ? 'El descuento de' : '';
    let config = new MatSnackBarConfig();
    config.duration = 2500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `${descText} Su Producto ${nombre} ha sido ${activado} exitosamente`,
      'x',
      config
    );
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

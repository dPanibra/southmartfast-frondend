import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductoService } from '@services/producto.service';
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
} from '@angular/forms';

import { Producto } from '@models/producto.model';
import { CategoriaService } from '@services/categoria.service';
import { UnidadMedidaService } from '@services/unidadMedida.services';
import { Categoria } from '@models/categoria.model';
import { UnidadMedida } from '@models/unidadMedida';
import { Imagen } from '@models/imagen.model';
import { ImagenService } from '@services/imagen.service';

import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from './../../../components/delete-dialog/delete-dialog.component';
import { environment } from './../../../../environments/environment';
import { MatFormField } from '@angular/material/form-field';

@Component({
  selector: 'app-edit-producto',
  templateUrl: './edit-producto.component.html',
  styleUrls: ['./edit-producto.component.scss'],
})
export class EditProductoComponent implements OnInit {
  @ViewChild('inputImg2') inputImg: MatFormField;

  load: boolean = true;
  edit: boolean = false;
  myForm: FormGroup;
  newProducto = new Producto(
    0,
    '',
    '',
    '',
    '',
    null,
    null,
    1,
    null,
    1,
    '',
    '',
    '',
    null,
    null,
    null,
    null,
    1,
    0,
    null,
    null,
    1,
    null,
    null,
    1
  );
  inputIMGdisable: boolean = true;

  categorias: Categoria[] = [];
  uniMedidas: UnidadMedida[] = [];

  btnState = {
    normal: true,
    disableBtn: false,
    loader: false,
    success: false,
  };

  // Variables Imagenes
  imgLocales: Imagen[] = [];
  urls: string[] = [];
  imgChange = { change: false, index: 0 };
  orinalUrls: string[] = [];

  toggleDesc: boolean = true;
  toggleStockCrit: boolean = true;
  toggleStockMin: boolean = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _sProducto: ProductoService,
    private _sCategoria: CategoriaService,
    private _sUnidadMedida: UnidadMedidaService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private _sImagen: ImagenService
  ) {
    this.getCategorias();
    this.getUnidadMedidas();
    this.route.params.subscribe((params) => {
      this.getProducto(params.id);
    });
    this.myForm = this.formBuilder.group({
      nombre: ['', [Validators.required, this.noWhitespaceValidator]],
      categoria: ['', [Validators.required]],
      unidad: ['', [Validators.required]],
      costo: ['', [Validators.required]],
      precio: ['', [Validators.required]],
      stock: ['', [Validators.required]],
      descripcion_corta: ['', [this.noWhitespaceValidator]],
      descripcion_larga: ['', [this.noWhitespaceValidator]],
      newInput: ['', [this.noWhitespaceValidator]],

    });
  }

  ngOnInit(): void {}

  getProducto(id: number) {
    this._sProducto.getProductoById(id).subscribe(
      (data: Producto) => {
        console.log(data);
        if (data) {
          this.newProducto = data;
          this.myForm.disable();
          this.load = false;
          this.fillImgLocales(data.images_url);
          this.toggleDesc=this.newProducto.descuento_porc==1?true:false;
          this.toggleStockCrit=this.newProducto.stockcrit_porc==1?true:false;
          this.toggleStockMin=this.newProducto.stockmin_porc==1?true:false;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  fillImgLocales(images_url: string) {
    console.log('llenar una vez');

    images_url != null
      ? images_url.split(',').forEach((url, i) => {
          let img: Imagen = {
            url,
            nombre: 'Imagen ' + (i == 0 ? 'Principal' : i + 1),
            imagen: '',
          };
          this.imgLocales.push(img);
          this.orinalUrls.push(img.url);
        })
      : '';
  }
  getCategorias() {
    this._sCategoria.getCategorias().subscribe(
      (data: Categoria[]) => {
        if (data) {
          this.categorias = data.sort((a, b) => a.n_orden - b.n_orden);
        }
      },
      (error) => console.log(error)
    );
  }
  getUnidadMedidas() {
    this._sUnidadMedida.getUnidadMedidas().subscribe(
      (data: UnidadMedida[]) => {
        if (data) {
          this.uniMedidas = data;
        }
      },
      (error) => console.log(error)
    );
  }
  editForm() {
    this.myForm.enable();
    this.edit = true;
    this.inputIMGdisable = false;
  }
  onSubmit() {
    if (this.myForm.valid === true) {
      this.btnState.disableBtn = true;
      this.btnState.normal = false;
      this.btnState.loader = true;
      this.inputIMGdisable = true;

      this.myForm.disable();
      this.newProducto.costo = +Number(this.newProducto.costo).toFixed(2);
      this.newProducto.precio = +Number(this.newProducto.precio).toFixed(2);
      this.newProducto.descuento_porc = this.toggleDesc == true ? 1 : 0;
      this.newProducto.stockmin_porc = this.toggleStockMin == true ? 1 : 0;
      this.newProducto.stockcrit_porc = this.toggleStockCrit == true ? 1 : 0;
      this.newProducto.descuento_act = this.newProducto.descuento_input > 0 ? 1 : 0;
      
      // console.log(this.newProducto);
      // REGISTRAR IMAGENES IF
      if (this.imgListsEquals()) {
        this.updateProducto();
      } else {
        this.updateImg();
      }
    }
  }

  updateProducto() {
    this._sProducto.editProducto(this.newProducto).subscribe(
      (data) => {
        if (data.message == 'success') {
          console.log(data);
          this.btnState.loader = false;
          this.btnState.success = true;
          this.openSnackBar();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  deleteIMG(deleteImg: Imagen, urlproduct: string) {
    deleteImg.url = null;
    this.newProducto[urlproduct] = null;
    console.log(this.newProducto);
  }

  getForm(img: Imagen): FormData {
    let formImg = new FormData();
    formImg.append('file', img.imagen);
    formImg.append('tipo', 'productos');
    return formImg;
  }

  goBack() {
    this.router.navigateByUrl('/productos');
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '400px',
      data: { type: 'Producto', name: this.newProducto.nombre },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === true) {
        this.deleteProducto(this.newProducto.id);
      }
    });
  }

  deleteProducto(id: number) {
    this._sProducto.deleteProducto(id).subscribe(
      (data) => {
        if (data.message == 'success') {
          this.openDeleteSnackBar();
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }
  openSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `Su Producto ${this.newProducto.nombre} se ha actualizado exitosamente`,
      'x',
      config
    );
    setTimeout(() => {
      this.goBack();
    }, 1500);
  }
  openDeleteSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['delete-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `El Producto ${this.newProducto.nombre} ha sido eliminado`,
      'x',
      config
    );
    setTimeout(() => {
      this.goBack();
    }, 1500);
  }
  noWhitespaceValidator(control: FormControl) {
    if (control.value == null) {
      const isValid = true;
      return isValid;
    }
    if (control.value.toString().length == 0) {
      const isValid = true;
      return isValid;
    } else {
      const isWhitespace = (control.value || '').trim().length == 0;
      const isValid = !isWhitespace;
      return isValid ? null : { whitespace: true };
    }
  }

  // Agregar Imagenes
  // Agregar Imagen
  inputImgClick() {
    const nImg = 25;
    this.imgLocales.length <= nImg
      ? this.inputImg._elementRef.nativeElement.click()
      : '';
  }
  changeImgClick(index: number) {
    this.imgChange = { change: true, index };
    this.inputImgClick();
  }

  readURL(event): void {
    if (event.target.files && event.target.files[0]) {
      if (this.imgChange.change) {
        let imgChange = this.imgLocales[this.imgChange.index];
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = (e) => (imgChange.url = reader.result);
        reader.readAsDataURL(file);
        imgChange.imagen = file;
        this.imgChange = { change: false, index: 0 };
      } else {
        let img: Imagen = { imagen: '', nombre: '', url: '' };
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = (e) => (img.url = reader.result);
        reader.readAsDataURL(file);
        img.imagen = file;
        img.nombre = `Imagen ${
          this.imgLocales.length > 0 ? this.imgLocales.length : 'Principal'
        }`;
        this.imgLocales.push(img);
      }
    }
  }
  updateImg() {
    const i: number = this.urls.length;

    if (this.imgLocales[i] != undefined) {
      const originalUrl: string =
        this.orinalUrls[i] != undefined ? this.orinalUrls[i] : '';
      const urlEquals: boolean = this.imgLocales[i].url == originalUrl;
      if (urlEquals) {
        this.urls.push(this.orinalUrls[i]);
        this.updateImg();
      } else {
        this.registerThisImg(this.imgLocales[i]);
      }
    } else {
      this.generarUrlsUpdate();
    }
  }
  registerThisImg(img: Imagen) {
    this._sImagen.registrarImagen(this.getForm(img)).subscribe(
      (data) => {
        let url: string = environment.baseURL + '/' + data.data;
        this.urls.push(url);
        console.log('Registrado:', url);
        if (this.urls.length == this.imgLocales.length) {
          this.generarUrlsUpdate();
        } else {
          this.updateImg();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  generarUrlsUpdate() {
    const urlsString: string = this.urls.join(',');
    this.newProducto.images_url = urlsString;
    console.log('String url images:', this.newProducto.images_url);

    this.updateProducto();
  }

  imgListsEquals(): boolean {
    let resp: boolean = true;
    if (this.imgLocales.length != this.orinalUrls.length) {
      return false;
    } else {
      this.imgLocales.forEach((img: Imagen, i) => {
        if (img.url != this.orinalUrls[i]) {
          resp = false;
          return;
        }
      });
      return resp;
    }
  }


    // Descuento, Stock Min - Crit

    desc() {
      if (this.toggleDesc) {
        let porcent =
          this.newProducto.precio * (this.newProducto.descuento_input / 100);
        this.newProducto.descuento = +Number(porcent).toFixed(2);
      } else {
        this.newProducto.descuento = this.newProducto.descuento_input;
      }
    }
    stocks() {
      this.stockMin();
      this.stockCrit();
    }
    stockMin() {
      if (this.toggleStockMin) {
        let porcent =
          this.newProducto.stock * (this.newProducto.stockmin_input / 100);
        this.newProducto.stockmin = +Number(porcent).toFixed(2);
      } else {
        this.newProducto.stockmin = this.newProducto.stockmin_input;
      }
    }
    stockCrit() {
      if (this.toggleStockCrit) {
        let porcent =
          this.newProducto.stock * (this.newProducto.stockcrit_input / 100);
        this.newProducto.stockcrit = +Number(porcent).toFixed(2);
      } else {
        this.newProducto.stockcrit = this.newProducto.stockcrit_input;
      }
    }
}

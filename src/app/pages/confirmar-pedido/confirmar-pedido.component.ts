import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Persona } from '@models/persona.model';
import { ProductoPedido } from '@models/productoPedido.model';
import { Pedido } from '@models/pedido.model';

import { PersonaService } from '@services/persona.service';
import { ProductoPedidoService } from '@services/productoPedido.service';
import { PedidoService } from '@services/pedido.service';

import { ProductCard } from '@models/productCard.inteface';

@Component({
  selector: 'app-confirmar-pedido',
  templateUrl: './confirmar-pedido.component.html',
  styleUrls: ['./confirmar-pedido.component.scss'],
})
export class ConfirmarPedidoComponent implements OnInit, OnDestroy {
  persona: Persona;
  personaOk: boolean = false;
  productosPedidos: ProductCard[];
  total: any = 0;
  observacion: string = '';
  btnState = {
    normal: true,
    disableBtn: false,
    loader: false,
    success: false,
  };
  suscribeServicePerson: Subscription;
  metodoPago='';
  distrito='';
  constructor(
    private router: Router,
    private _sPersona: PersonaService,
    private _sProductoPedido: ProductoPedidoService,
    private _sPedido: PedidoService,
  ) {
    
  }
  ngOnInit(): void {
    if (this._sPersona.personValidator) {
      this.persona = this._sPersona.newPerson;
      this.personaOk = true;
      if (this._sProductoPedido.productosCard) {
        this.productosPedidos = this._sProductoPedido.productosCard;
        this.observacion = this._sPedido.pedidoActual.observacion;
        this.total =Number(this._sPedido.pedidoActual.pagoTotal).toFixed(2) ;
        this.metodoPago=this._sPedido.pedidoActual.metodoPago;
        this.distrito=this._sPersona.newPerson.distrito;
      }
    }
  }
  ngOnDestroy(): void {
    this.suscribeServicePerson.unsubscribe();
  }
  goToFinish() {
    this.btnState.disableBtn = true;
    this.btnState.normal = false;
    this.btnState.loader = true;
    let savePerson = this._sPersona.newPerson;
    this.registerPerson(savePerson);
  }
  goToBack() {
    let notPerson: Persona = new Persona(
      0,'','','','','','','',0,0,''
    );
    let notPedido: Pedido = new Pedido(
      0,'','','','',0,0,'Pendiente','','','','','','',0,0,'',0
    );
    this._sProductoPedido.productosCard.length = 0;
    this._sPedido.pedidoActual = notPedido;
    this._sPersona.newPerson = notPerson;
    this._sPersona.personValidator = false;
    this._sPedido.updateDelivery(undefined);
    this._sPedido.updateTotalPedido();
    this.router.navigateByUrl('');
}

  registerPerson(newPerson: Persona) {
    let newPedido: Pedido = this._sPedido.pedidoActual;

    this.suscribeServicePerson = this._sPersona
      .registrarPersona(newPerson)
      .subscribe(
        (data) => {
          if (data.message == 'success') {
            let dataPerson: Persona = data.data;
            newPedido.persona_id = dataPerson.id;
            this._sPersona.newPerson = dataPerson;
            console.log('1', data);

            this.registrarPedido(newPedido);
          }
        },
        (error) => {console.error(error)}
      );
  }
  registrarPedido(newPedido: Pedido) {
    this._sPedido.registrarPedido(newPedido).subscribe(
      (data) => {
        if (data.message == 'success') {
          console.log('2', data);
          let dataPedido: Pedido = data.data;
          this._sPedido.pedidoActual=dataPedido;
          let productosPedidos = this.generarProductosPedidos(data.data.id);
          this.registrarProductosPedidos(productosPedidos);
        }
      },
      (error) => {console.error(error)}
    );
  }

  registrarProductosPedidos(productosPedidos: any) {
    this._sProductoPedido.registrarProductoPedido(productosPedidos).subscribe(
      (data) => {
        if (data.message == 'success') {
          console.log('3', data);
          this.btnState.loader = false;
          this.btnState.success = true;
          setTimeout(() => {
            this.router.navigateByUrl('/finalizarPedido');
          }, 500);
        }
      },
      (error) => {console.error(error)}
    );
  }
  generarProductosPedidos(id) {
    let ppedidos = {
      productos_pedidos: [],
    };
    this._sProductoPedido.productosCard.forEach((element) => {
      let productPedido = new ProductoPedido(0,'','',element.cantidad, +element.total,element.id,id,null);
      ppedidos.productos_pedidos.push(productPedido);
    });
    console.log(ppedidos);
    return ppedidos;
  }
}

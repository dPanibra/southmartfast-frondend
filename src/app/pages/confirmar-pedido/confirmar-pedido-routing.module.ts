import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ConfirmarPedidoComponent} from './confirmar-pedido.component'
const routes: Routes = [
  {
    path:"",
    component:ConfirmarPedidoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfirmarPedidoRoutingModule {}

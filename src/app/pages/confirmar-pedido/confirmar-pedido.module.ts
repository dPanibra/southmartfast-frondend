import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmarPedidoRoutingModule } from './confirmar-pedido-routing.module';
import { MaterialModule } from './../../material.module';
import { ConfirmarPedidoComponent } from './confirmar-pedido.component';

@NgModule({
  declarations: [ConfirmarPedidoComponent],
  imports: [CommonModule, ConfirmarPedidoRoutingModule, MaterialModule],
})
export class ConfirmarPedidoModule {}

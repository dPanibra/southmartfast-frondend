import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriasRoutingModule } from './categorias-routing.module';
import { CategoriasComponent } from './categorias.component';
import { MaterialModule} from './../../material.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddCategoriaComponent } from './add-categoria/add-categoria.component';
import { EditCategoriaComponent } from './edit-categoria/edit-categoria.component';

@NgModule({
  declarations: [CategoriasComponent, AddCategoriaComponent, EditCategoriaComponent],
  imports: [
    CommonModule,
    CategoriasRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CategoriasModule { }

import { Component, OnInit } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { CategoriaService } from '@services/categoria.service';
import { Categoria } from '@models/categoria.model';
import { Producto } from '@models/producto.model';
import { ProductoService } from '@services/producto.service';

export interface CategoriaTabla {
  id: number;
  nombre: string;
  img: string;
  activado: boolean;
  toggleDisable: boolean;
  productos: Producto[];
}

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss'],
})
export class CategoriasComponent implements OnInit {
  loading: boolean = true;
  listCategorias: Categoria[] = [];
  listCategoriasTabla: CategoriaTabla[] = [];
  listProductos: Producto[] = [];

  constructor(
    private _sCategoria: CategoriaService,
    private _sProducto: ProductoService,
    private _snackBar: MatSnackBar
  ) {}
  ngOnInit(): void {
    this._sCategoria.getCategoriasWithProducts().subscribe(
      (data: Categoria[]) => {
        // console.log(data);
        this.listCategorias = data.sort((a, b) => a.n_orden - b.n_orden);
        this.listCategorias.forEach((elem) => {
          elem.productos.forEach((productin: Producto) => {
            this.listProductos.push(productin);
          });
          let newCategoriaTabla: CategoriaTabla = {
            id: elem.id,
            nombre: elem.nombre,
            img: elem.url_img,
            activado: elem.activo == 1 ? true : false,
            toggleDisable: false,
            productos: elem.productos,
          };
          this.listCategoriasTabla.push(newCategoriaTabla);
        });
        this.loading = false;
      },
      (error) => {
        console.log(error);
      }
    );
  }
  toggle(cate: CategoriaTabla) {
    cate.toggleDisable = true;
    let categoriaEdit: Categoria = this.listCategorias.find(
      (element) => element.id == cate.id
    );
    categoriaEdit.activo = cate.activado ? 1 : 0;
    this._sCategoria.editCategoria(categoriaEdit).subscribe(
      (data) => {
        if (data.message == 'success') {
          cate.toggleDisable = false;
          this.openSnackBar(
            categoriaEdit.nombre,
            categoriaEdit.activo,
            'Categoria'
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  drop(event: CdkDragDrop<string[]>, categoriaEvent: CategoriaTabla) {
    const nombre = event.item.element.nativeElement.innerText;
    let productoEdit: Producto = this.listProductos.find(
      (product: Producto) => {
        return product.nombre === nombre;
      }
    );
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      productoEdit.categoria_id = categoriaEvent.id;
      this._sProducto.editProducto(productoEdit).subscribe(
        (data) => {
          this.openSnackBar(
            productoEdit.nombre,
            productoEdit.activo,
            'Producto'
          );
        },
        (error) => console.log(error)
      );
    }
  }

  drop2(event: CdkDragDrop<string[]>) {
    const id = +event.item.element.nativeElement.id;
    if (event.currentIndex < event.previousIndex) {
      let listCategoriasEdit: Categoria[] = this.listCategorias
        .filter((cate: Categoria) => {
          return (
            cate.n_orden > event.currentIndex &&
            cate.n_orden <= event.previousIndex + 1
          );
        })
        .sort((a, b) => a.n_orden - b.n_orden);
      let asd = listCategoriasEdit.map((cate: Categoria) => {
        if (cate.id == id) {
          cate.n_orden = event.currentIndex + 1;
        } else {
          cate.n_orden = cate.n_orden + 1;
        }
        return cate;
      });
      asd.sort((a, b) => a.n_orden - b.n_orden);
      this.actualizarListaCategorias(asd);
    } else if (event.currentIndex > event.previousIndex) {
      let listCategoriasEdit: Categoria[] = this.listCategorias
        .filter((cate: Categoria) => {
          return (
            cate.n_orden <= event.currentIndex + 1 &&
            cate.n_orden >= event.previousIndex + 1
          );
        })
        .sort((a, b) => a.n_orden - b.n_orden);
      let asd = listCategoriasEdit.map((cate: Categoria) => {
        if (cate.id == id) {
          cate.n_orden = event.currentIndex + 1;
        } else {
          cate.n_orden = cate.n_orden - 1;
        }
        return cate;
      });
      asd.sort((a, b) => a.n_orden - b.n_orden);
      this.actualizarListaCategorias(asd);
    }

    moveItemInArray(
      this.listCategoriasTabla,
      event.previousIndex,
      event.currentIndex
    );
  }

  actualizarCategoria(cate: Categoria) {
    this._sCategoria.editCategoria(cate).subscribe(
      (data) => {},
      (error) => {
        console.log(error);
      }
    );
  }
  actualizarListaCategorias(listCates: Categoria[]) {
    const numList = listCates.length;
    let contLista = 0;
    for (let i = 0; i < listCates.length; i++) {
      this._sCategoria.editCategoria(listCates[i]).subscribe(
        (data) => {
          contLista++;
          if (numList == contLista + 1) {
            this.openSnackBarOrden();
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }
  openSnackBarOrden() {
    let config = new MatSnackBarConfig();
    config.duration = 2500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      'El Orden de sus Categorias ha sido actualizado',
      'x',
      config
    );
  }
  openSnackBar(nombre: string, activo: number, tipo: string) {
    const activado = activo == 1 ? 'Activada' : 'Desactivada';
    let mensaje: string =
      tipo == 'Producto'
        ? `Su Producto ${nombre} ha sido actualizado exitosamente`
        : `Su Categoria ${nombre} ha sido ${activado} exitosamente`;
    let config = new MatSnackBarConfig();
    config.duration = 2500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(mensaje, 'x', config);
  }
}

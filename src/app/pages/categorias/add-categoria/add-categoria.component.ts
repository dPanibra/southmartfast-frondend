import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { Categoria } from '@models/categoria.model';
import { CategoriaService } from '@services/categoria.service';

import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { Imagen } from '@models/imagen.model';
import { ImagenService } from '@services/imagen.service';

import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-add-categoria',
  templateUrl: './add-categoria.component.html',
  styleUrls: ['./add-categoria.component.scss'],
})
export class AddCategoriaComponent implements OnInit {
  myForm: FormGroup;
  newCategoria: Categoria = new Categoria(0, '', '', '', null, 1, 0, null);
  newImagen = new Imagen(null, '', 'categorias');
  img: any = null;
  inputIMGdisable: boolean = false;
  btnState = {
    normal: true,
    disableBtn: false,
    loader: false,
    success: false,
  };
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private _sImagen: ImagenService,
    private _snackBar: MatSnackBar,
    private _sCategoria: CategoriaService
  ) {}

  ngOnInit(): void {
    this.myForm = this.formBuilder.group({
      nombre: ['', [Validators.required, this.noWhitespaceValidator]],
    });
  }
  onSubmit() {
    if (this.myForm.valid === true) {
      this.inputIMGdisable = true;
      this.myForm.disable();
      // console.log(this.newProducto);
      if (this.img != null) {
        this.registerIMG();
      } else {
        this.registrarCategoria(this.newCategoria);
      }
    }
  }
  registrarCategoria(newCategoria: Categoria) {
    this.btnState.disableBtn = true;
    this.btnState.normal = false;
    this.btnState.loader = true;
    this._sCategoria.registrarCategoria(newCategoria).subscribe(
      (data) => {
        if (data.message == 'success') {
          console.log(data);
          this.btnState.loader = false;
          this.btnState.success = true;
          this.openSnackBar();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  readURL(event): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = (e) => (this.img = reader.result);
      reader.readAsDataURL(file);
      this.newImagen.imagen = file;
    }
  }

  registerIMG() {
    let form = new FormData();
    form.append('file', this.newImagen.imagen);
    form.append('tipo', 'categorias');
    this._sImagen.registrarImagen(form).subscribe(
      (data) => {
        this.newCategoria.url_img = environment.baseURL + '/' + data.data;
        this.registrarCategoria(this.newCategoria);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  openSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `La Categoria ${this.newCategoria.nombre} se ha registrado exitosamente`,
      'x',
      config
    );
    setTimeout(() => {
      this.goBack();
    }, 1500);
  }

  goBack() {
    this.router.navigateByUrl('/categorias');
  }
  noWhitespaceValidator(control: FormControl) {
    if (control.value.toString().length == 0) {
      const isValid = true;
      return isValid;
    } else {
      const isWhitespace = (control.value || '').trim().length == 0;
      const isValid = !isWhitespace;
      return isValid ? null : { whitespace: true };
    }
  }
}

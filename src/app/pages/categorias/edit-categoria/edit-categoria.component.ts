import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { Categoria } from '@models/categoria.model';
import { CategoriaService } from '@services/categoria.service';

import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { Imagen } from '@models/imagen.model';
import { ImagenService } from '@services/imagen.service';

import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from './../../../components/delete-dialog/delete-dialog.component';

import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-edit-categoria',
  templateUrl: './edit-categoria.component.html',
  styleUrls: ['./edit-categoria.component.scss'],
})
export class EditCategoriaComponent implements OnInit {
  myForm: FormGroup;
  newCategoria: Categoria = new Categoria(0, '', '', '', null, 1, 0, null);
  newImagen = new Imagen(null, '', 'categorias');
  img: any = null;
  inputIMGdisable: boolean = true;
  btnState = {
    normal: true,
    disableBtn: false,
    loader: false,
    success: false,
  };
  load: boolean = true;
  edit: boolean = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private _sImagen: ImagenService,
    private _snackBar: MatSnackBar,
    private _sCategoria: CategoriaService,
    public dialog: MatDialog
  ) {
    this.route.params.subscribe((params) => {
      this.getCategoria(params.id);
    });
  }

  ngOnInit(): void {
    this.myForm = this.formBuilder.group({
      nombre: ['', [Validators.required, this.noWhitespaceValidator]],
    });
  }
  getCategoria(id: number) {
    this._sCategoria.getCategoriaById(id).subscribe(
      (data: Categoria) => {
        console.log(data);
        if (data) {
          this.newCategoria = data;
          this.myForm.disable();
          this.load = false;
          this.img = data.url_img;
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  editForm() {
    this.myForm.enable();
    this.edit = true;
    this.inputIMGdisable = false;
  }

  onSubmit() {
    if (this.myForm.valid === true) {
      this.btnState.disableBtn = true;
      this.btnState.normal = false;
      this.btnState.loader = true;
      this.inputIMGdisable = true;
      this.myForm.disable();
      // console.log(this.newProducto);
      if (this.img != this.newCategoria.url_img) {
        this.registerIMG();
      } else {
        this.updateCategoria(this.newCategoria);
      }
    }
  }
  updateCategoria(newCategoria: Categoria) {
    this._sCategoria.editCategoria(newCategoria).subscribe(
      (data) => {
        if (data.message == 'success') {
          console.log(data);
          this.btnState.loader = false;
          this.btnState.success = true;
          this.openSnackBar();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  readURL(event): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = (e) => (this.img = reader.result);
      reader.readAsDataURL(file);
      this.newImagen.imagen = file;
    }
  }

  registerIMG() {
    let form = new FormData();
    form.append('file', this.newImagen.imagen);
    form.append('tipo', 'categorias');
    this._sImagen.registrarImagen(form).subscribe(
      (data) => {
        this.newCategoria.url_img = environment.baseURL + '/' + data.data;
        this.updateCategoria(this.newCategoria);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  openSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `La Categoria ${this.newCategoria.nombre} se ha actualizado exitosamente`,
      'x',
      config
    );
    setTimeout(() => {
      this.goBack();
    }, 1500);
  }

  goBack() {
    this.router.navigateByUrl('/categorias');
  }

  openDeleteSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['delete-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `La Categoria ${this.newCategoria.nombre} ha sido eliminada`,
      'x',
      config
    );
    setTimeout(() => {
      this.goBack();
    }, 1500);
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '400px',
      data: { type: 'Categoria', name: this.newCategoria.nombre },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === true) {
        this.deleteProducto(this.newCategoria.id);
      }
    });
  }

  deleteProducto(id: number) {
    this._sCategoria.deleteCategoria(id).subscribe(
      (data) => {
        if (data.message == 'success') {
          this.openDeleteSnackBar();
        }
      },
      (error) => {
        console.error(error);
      }
    );
  }
  noWhitespaceValidator(control: FormControl) {
    if (control.value.toString().length == 0) {
      const isValid = true;
      return isValid;
    } else {
      const isWhitespace = (control.value || '').trim().length == 0;
      const isValid = !isWhitespace;
      return isValid ? null : { whitespace: true };
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { Config } from '@models/config.model';
import { ConfigService } from '@services/config.service';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss'],
})
export class GeneralComponent implements OnInit {
  stateGeneral = {
    loading: true,
    edit: false,
  };

  btnState = {
    normal: true,
    disableBtn: false,
    loading: false,
    success: false,
  };

  generalConfig = new Config(1, '', '', '', '', '');

  myForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private _sConfig: ConfigService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {
    this.myForm = this.formBuilder.group({
      nombreEmpresa: ['', [Validators.required, this.noWhitespaceValidator]],
      textoCabecera: ['', [Validators.required, this.noWhitespaceValidator]],
      correoAvisos: ['', [Validators.required, Validators.email]],
      celSMS: ['', [Validators.required]],
      celPagos: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.myForm.disable();
    this._sConfig.getConfig().subscribe((data: Config) => {
      if (data) {
        this.stateGeneral.loading=false;
        this.generalConfig = data;
      }
    });
  }

  edit() {
    this.stateGeneral.edit = true;
    this.myForm.enable();
  }
  onSubmit() {
    if (this.myForm.valid) {
      this.myForm.disable();
      this.btnState.disableBtn = true;
      this.btnState.normal = false;
      this.btnState.loading = true;
      this._sConfig.editConfig(this.generalConfig).subscribe(
        (data) => {
          if (data) {
            this.btnState.loading = false;
            this.btnState.success = true;
            this.openSnackBar();
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }
  openSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3500;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'right';
    config.verticalPosition = 'bottom';
    this._snackBar.open(
      `Su Configuración General se ha actualizada exitosamente`,
      'x',
      config
    );
    setTimeout(() => {
      this.goBack();
    }, 1500);
  }
  goBack() {
    this.router.navigateByUrl('/');
  }
  noWhitespaceValidator(control: FormControl) {
    if (control.value.toString().length == 0) {
      const isValid = true;
      return isValid;
    } else {
      const isWhitespace = (control.value || '').trim().length == 0;
      const isValid = !isWhitespace;
      return isValid ? null : { whitespace: true };
    }
  }
}

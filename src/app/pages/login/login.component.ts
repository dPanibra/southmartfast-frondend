import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import {LoginService} from '@services/login.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  hide = true;
  myForm: FormGroup;
  usuario = {
    nusuario: '',
    contrasena: '',
  };
  btnState = {
    normal: true,
    disableBtn: false,
    loader: false,
    success: false,
  };
  constructor(private formBuilder: FormBuilder,private _sLogin:LoginService,private router: Router) {
    this.myForm = this.formBuilder.group({
      nususario: [
        '',
        [
          Validators.required,
          Validators.minLength(4),
          this.noWhitespaceValidator,
        ],
      ],
      contrasena: [
        '',
        [
          Validators.required,
          Validators.minLength(4),
          this.noWhitespaceValidator,
        ],
      ],
    });
  }
  noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }
  ngOnInit(): void {}

  onSubmit() {
    if (this.myForm.valid) {
      this.btnState.disableBtn = true;
      this.btnState.loader = true;
      this.btnState.normal = false;
      setTimeout(() => {
        this.logIn();
      }, 700);
    }
  }

  logIn(): void {
    if (
      this.usuario.nusuario == 'adminBodega' &&
      this.usuario.contrasena == 'admin123'
    ) {
      this.btnState.loader= false;
      this.btnState.success = true;
      setTimeout(() => {
        this.loged();
      }, 300);
    } else {
      this.btnState.disableBtn = false;
      this.btnState.loader = false;
      this.btnState.normal = true;
      this.usuario.contrasena='';
    }
  }
  loged(){
    this._sLogin.updateisLoged(true);
    this.router.navigateByUrl('/');
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

import { Mensaje } from '@models/mensaje.model';

import { PersonaService } from '@services/persona.service';
import { ProductoPedidoService } from '@services/productoPedido.service';
import { PedidoService } from '@services/pedido.service';
import { MensajeService } from '@services/mensaje.services';
import { Persona } from '@models/persona.model';
import { Pedido } from '@models/pedido.model';
import { ConfigService } from '@services/config.service';

@Component({
  selector: 'app-finalizar-pedido',
  templateUrl: './finalizar-pedido.component.html',
  styleUrls: ['./finalizar-pedido.component.scss'],
})
export class FinalizarPedidoComponent implements OnInit {
  total: any = 0;
  tipo = '';
  whatsapp: string = '';
  numberwsp: string = '';
  load: boolean = false;
  constructor(
    private router: Router,
    private _snackBar: MatSnackBar,
    private _sPersona: PersonaService,
    private _sProductoPedido: ProductoPedidoService,
    private _sPedido: PedidoService,
    private _sMensaje: MensajeService,
    private _sConfig: ConfigService
  ) {
    console.log('Constructor');
  }

  ngOnInit(): void {
    console.log('onInit');
    this._sConfig.getConfig().subscribe((data) => {
      if (data) {
        console.log(data);
        this.load = true;
        this.numberwsp = data.celSMS;
        this.createLinkWsp(data.celSMS);
        this.smsForCompany(data.celSMS);
        this.smsForClient();
      }
    });
    this.total = Number(this._sPedido.pedidoActual.pagoTotal).toFixed(2);
    this.tipo = this._sPedido.pedidoActual.metodoPago;
  }
  createLinkWsp(numberWhatsapp) {
    let pedidos = '';
    this._sProductoPedido.productosCard.forEach((element) => {
      pedidos += `${element.cantidad} x ${element.nombre} a ${Number(
        element.total
      ).toFixed(2)},%0d%0a`;
    });
    let tott = Number(this._sPedido.pedidoActual.pagoTotal).toFixed(2);
    let referencia = '';
    if (this._sPedido.pedidoActual.cl_referencia != null) {
      referencia = `%20con%20referencia%20${this._sPedido.pedidoActual.cl_referencia}`;
    }
    this.whatsapp = `https://api.whatsapp.com/send?phone=51${numberWhatsapp}&text=Hola%20SouthMartFast,%20mi%20pedido%20con%20ID%20${this._sPedido.pedidoActual.id}%20con%20los%20items:%0d%0a${pedidos}por%20S/${tott}%20a%20la%20direcci%C3%B3n:%20${this._sPedido.pedidoActual.cl_direccion}%20-%20*${this._sPedido.pedidoActual.cl_distrito}*%20${referencia}%20a%20sido%20realizado%20y%20se%20pagara%20por%20${this._sPedido.pedidoActual.metodoPago}.%20Coordinemos%20el%20Servicio%20Extra%20de%20Delivery`;
    console.log(this.whatsapp);
  }
  goBack() {
    let notPerson: Persona = new Persona(
      0,
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      0,
      0,
      ''
    );
    let notPedido: Pedido = new Pedido(
      0,
      '',
      '',
      '',
      '',
      0,
      0,
      'Pendiente',
      '',
      '',
      '',
      '',
      '',
      '',
      0,
      0,
      '',
      0
    );
    this._sProductoPedido.productosCard.length = 0;
    this._sPedido.pedidoActual = notPedido;
    this._sPersona.newPerson = notPerson;
    this._sPersona.personValidator = false;
    this.router.navigateByUrl('');
  }
  smsForClient() {
    let mensajeClient: string = `Hola ${this._sPersona.newPerson.nombre}, su pedido de S/${this._sPedido.pedidoActual.pagoTotal} ha sido realizado`;
    let telefonoClient: string = `51${this._sPersona.newPerson.telefono}`;
    let smsClient = new Mensaje(
      0,
      '',
      '',
      mensajeClient,
      telefonoClient,
      0,
      this._sPersona.newPerson.id,
      this._sPedido.pedidoActual.id
    );
    // console.log(smsClient);
    this._sMensaje.registrarMensaje(smsClient).subscribe(
      (data) => {
        console.log(data);
        if (data.message == 'success') {
          this.openSnackBar();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  smsForCompany(telefonoCompany) {
    let created = this._sPedido.pedidoActual.created_at;
    let fecha = created.substring(0, created.indexOf(' '));
    let hora = created.substring(created.indexOf(' '), created.length);
    let mensajeCompany: string = `Hola tiene un nuevo pedido de S/${this._sPedido.pedidoActual.pagoTotal} generado el ${fecha} a las ${hora} con ID:${this._sPedido.pedidoActual.id}`;
    let smsCompany = new Mensaje(
      0,
      '',
      '',
      mensajeCompany,
      `51${telefonoCompany}`,
      1,
      this._sPersona.newPerson.id,
      this._sPedido.pedidoActual.id
    );
    console.log(smsCompany);
    this._sMensaje.registrarMensaje(smsCompany).subscribe((data) => {
      if (data.message == 'success') {
      }
    });
  }

  openSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 7000;
    config.panelClass = ['success-snackbar'];
    config.horizontalPosition = 'center';
    config.verticalPosition = 'top';
    this._snackBar.open('Su pedido ha sido realizado', 'x', config);
  }
}

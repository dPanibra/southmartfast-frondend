import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule} from './../../material.module';
import { FinalizarPedidoRoutingModule } from './finalizar-pedido-routing.module';
import { FinalizarPedidoComponent } from "./finalizar-pedido.component";

@NgModule({
  declarations: [FinalizarPedidoComponent],
  imports: [
    CommonModule,
    FinalizarPedidoRoutingModule,
    MaterialModule
  ]
})
export class FinalizarPedidoModule { }

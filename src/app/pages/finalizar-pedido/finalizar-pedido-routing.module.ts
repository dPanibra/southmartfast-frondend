import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FinalizarPedidoComponent} from './finalizar-pedido.component'
const routes: Routes = [
  {
    path:"",
    component:FinalizarPedidoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinalizarPedidoRoutingModule { }

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HomeService } from '@services/home.service';
import { ScrollService } from '@services/scroll.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(private _sScroll: ScrollService, private _sHome: HomeService) {}

  ngOnInit(): void {
    window.onload = () => {
      window.addEventListener('scroll', () => {
        this._sScroll.updatePlace();
      });
    };
  }
}

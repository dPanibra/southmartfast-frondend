import { Component, OnInit } from '@angular/core';
import { PedidoService } from '@services/pedido.service';
import { ProductoPedidoService } from '@services/productoPedido.service';
import { HomeService } from '@services/home.service';

@Component({
  selector: 'app-total-details-home',
  templateUrl: './total-details-home.component.html',
  styleUrls: ['./total-details-home.component.scss'],
})
export class TotalDetailsHomeComponent implements OnInit {

  open: boolean;
  total:any='0.00';
  distrito='';
  precioDelivery='0.00';
  
  constructor(
    public _sPedido: PedidoService,
    public _sProductoPedido: ProductoPedidoService,
    private _sHome: HomeService
  ) {}

  ngOnInit(): void {
    this._sHome.getOpen().subscribe((isOpen) => {
      this.open=isOpen;    
    });
    this._sPedido.getTotalPedido().subscribe(totalPedido=>{
      this.total=totalPedido;
    })
    this._sPedido.getDelivery().subscribe(delivery=>{
      this.distrito=delivery.name;
      this.precioDelivery=delivery.precio;
    })
  }
  cerrarDetalles(){
    this._sHome.updateOpen(false);
  }
}

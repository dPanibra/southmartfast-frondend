import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalDetailsHomeComponent } from './total-details-home.component';

describe('TotalDetailsHomeComponent', () => {
  let component: TotalDetailsHomeComponent;
  let fixture: ComponentFixture<TotalDetailsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalDetailsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalDetailsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

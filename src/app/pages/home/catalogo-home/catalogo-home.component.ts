import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';

import { ProductoPedidoService } from '@services/productoPedido.service';
import { PedidoService } from '@services/pedido.service';
import { HomeService } from '@services/home.service';
import { ProductCard } from '@models/productCard.inteface';

import { CategoriaService } from '@services/categoria.service';
import { Categoria } from '@models/categoria.model';

import { UnidadMedida } from '@models/unidadMedida';
import { UnidadMedidaService } from '@services/unidadMedida.services';

import { MatDialog } from '@angular/material/dialog';
import { ImageDialogComponent } from './../../../components/image-dialog/image-dialog.component';
import { Producto } from '@models/producto.model';

import { ConfigService } from '@services/config.service';

export interface CategoriasTabla {
  nombre: string;
  productos: ProductCard[];
}
@Component({
  selector: 'app-catalogo-home',
  templateUrl: './catalogo-home.component.html',
  styleUrls: ['./catalogo-home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CatalogoHomeComponent implements OnInit, OnDestroy {
  unidadesMedidas: UnidadMedida[] = [];
  categorias: CategoriasTabla[] = [];
  newCategorias: any;

  textoCabecera: string;

  suUnidadMedida: Subscription;
  suCateProductos: Subscription;

  load: boolean = false;
  listProducts: Producto[] = [];
  default = 'http://api.store.southmartfast.com/images/productos/default.jpeg';

  constructor(
    private _sProductoPedidoService: ProductoPedidoService,
    private _sPedidoService: PedidoService,
    private _sConfig: ConfigService,
    private _sCategoria: CategoriaService,
    private _sUnidadMedida: UnidadMedidaService,
    public dialog: MatDialog,
    private sanitization: DomSanitizer
  ) {
    this.getUnidadesMedidas();
  }

  ngOnInit(): void {
    this._sConfig.getGeneralConfig().subscribe((generalConfig) => {
      this.textoCabecera = generalConfig.textoCabecera;
    });
  }
  ngOnDestroy(): void {
    this.suCateProductos.unsubscribe();
    this.suUnidadMedida.unsubscribe();
  }
  getUnidadesMedidas() {
    this.suUnidadMedida = this._sUnidadMedida.getUnidadMedidas().subscribe(
      (data: UnidadMedida[]) => {
        if (data) {
          this.unidadesMedidas = data;
          this.getCategoriaWithProducts();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  getCategoriaWithProducts() {
    this.suCateProductos = this._sCategoria
      .getCategoriasWithProducts()
      .subscribe(
        (data: Categoria[]) => {
          data
            .sort((a, b) => a.n_orden - b.n_orden)
            .filter((elem) => elem.activo === 1)
            .forEach((element) => {
              let productos: ProductCard[] = [];
              element.productos.forEach((elem: Producto) => {
                this.listProducts.push(elem);
                if (elem.stock != 0 && elem.activo != 0) {
                  let producto: ProductCard = {
                    id: elem.id,
                    nombre: elem.nombre,
                    descripcion: elem.descripcion_corta,
                    cantidad_venta: elem.cantidad_venta,
                    img: this.getImg(elem.images_url),
                    unidad: this.unidadesMedidas.find(
                      (uni) => uni.id === elem.unidad_medida_id
                    ).nombre,
                    precio: Number(elem.precio).toFixed(2),
                    cantidad: 0,
                    total: 0,
                    descuento: elem.descuento,
                    descuento_act: elem.descuento_act == 1 ? true : false,
                    descuento_input:
                      elem.descuento_porc == 1
                        ? elem.descuento_input
                        : Number(elem.descuento_input).toFixed(2),
                    descuento_porc: elem.descuento_porc == 1 ? true : false,
                    stock: elem.stock,
                  };
                  productos.push(producto);
                  // console.log('img', producto.img);
                }
              });
              let categoria: CategoriasTabla = {
                nombre: element.nombre.toUpperCase(),
                productos: productos,
              };
              this.categorias.push(categoria);
            });
          this.load = true;
        },
        (error) => {
          console.log(error);
        }
      );
  }
  getImg(images: string) {
    return images == null
      ? this.default
      : images.split(',')[0] == ''
      ? this.default
      : images.split(',')[0];
  }

  add(producto: ProductCard, a) {
    if (a == 'menos') {
      producto.cantidad--;
      if (producto.cantidad <= 0) {
        producto.cantidad = 0;
        producto.total = 0;
        this.quitarProductoPedido(producto.id);
      } else {
        this.actualizarProductoPedido(producto);
      }
    } else if (a == 'mas' && producto.cantidad < producto.stock) {
      producto.cantidad++;
      this.actualizarProductoPedido(producto);
    }
    this._sPedidoService.updateTotalPedido();
  }

  quitarProductoPedido(id: number) {
    let newlist = this._sProductoPedidoService.productosCard.filter(
      (productoP) => productoP.id != id
    );
    this._sProductoPedidoService.productosCard = newlist;
  }
  actualizarProductoPedido(producto: ProductCard) {
    const precioFinal =
      producto.descuento_act && producto.descuento > 0
        ? producto.precio - producto.descuento
        : producto.precio;
    producto.total = producto.cantidad * precioFinal;
    producto.total = Number(producto.total).toFixed(2);
    let productoPedido = this._sProductoPedidoService.productosCard.filter(
      (product) => product.id == producto.id
    )[0];
    if (productoPedido) {
      productoPedido.cantidad = producto.cantidad;
      productoPedido.total = producto.total;
    } else {
      this._sProductoPedidoService.productosCard.push(producto);
    }
  }

  openDialog(ID): void {
    const product: Producto = this.listProducts.find((elem) => elem.id == ID);
    const imagenes: any[] =
      product.images_url == null ? [] : product.images_url.split(',');
    if (imagenes.length > 0 && imagenes[0] != '') {
      const dialogRef = this.dialog.open(ImageDialogComponent, {
        width: '700px',
        data: { imagenes, descripcion: product.descripcion_larga },
      });
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from './../../material.module';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HeaderHomeComponent } from './header-home/header-home.component';
import { CatalogoHomeComponent } from './catalogo-home/catalogo-home.component';
import { FormularioHomeComponent } from './formulario-home/formulario-home.component';
import { FooterHomeComponent } from './footer-home/footer-home.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TotalHomeComponent } from './total-home/total-home.component';
import { TotalDetailsHomeComponent } from './total-details-home/total-details-home.component';

@NgModule({
  declarations: [
    HomeComponent,
    HeaderHomeComponent,
    CatalogoHomeComponent,
    FormularioHomeComponent,
    FooterHomeComponent,
    TotalHomeComponent,
    TotalDetailsHomeComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class HomeModule {}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PedidoService } from '@services/pedido.service';
import { ProductoPedidoService } from '@services/productoPedido.service';
import { HomeService } from '@services/home.service';
import { ScrollService } from '@services/scroll.service';
@Component({
  selector: 'app-total-home',
  templateUrl: './total-home.component.html',
  styleUrls: ['./total-home.component.scss'],
})
export class TotalHomeComponent implements OnInit {
  @ViewChild('compra') compraElem: ElementRef;
  @ViewChild('entrega') entregaElem: ElementRef;
  @ViewChild('pago') pagoElem: ElementRef;
  @ViewChild('formHome') formHome: ElementRef;
  @ViewChild('formMetodo') formMetodo: ElementRef;
  compraState = {
    active: true,
    up: false,
  };
  entregaState = {
    active: false,
    down: true,
    up: false,
  };
  pagoState = {
    active: false,
    up: true,
  };
  total: any = '0.00';
  constructor(
    public _sPedido: PedidoService,
    public _sProductoPedido: ProductoPedidoService,
    private _sHome: HomeService,
    private _sScroll: ScrollService
  ) {}

  ngOnInit(): void {
    this._sPedido.getTotalPedido().subscribe((totalPedido) => {
      this.total = totalPedido;
    });
    this._sScroll.getPlace().subscribe((place) => {
      if (place == 'Compra') {
        this.compra();
      } else if (place == 'Entrega') {
        this.entrega();
      } else if (place == 'Pago') {
        this.pago();
      }
    });
  }
  abrirDetalles() {
    if (this._sProductoPedido.productosCard.length > 0) {
      this._sHome.updateOpen(true);
    }
  }
  compra() {
    this.compraElem.nativeElement.classList.add('optionActive');
    this.entregaElem.nativeElement.classList.remove('optionActive');
    this.pagoElem.nativeElement.classList.remove('optionActive');
    this.compraState.active = true;
    this.compraState.up = false;
    this.entregaState.active = false;
    this.entregaState.up = false;
    this.entregaState.down = true;
    this.pagoState.active = false;
    this.pagoState.up = true;
  }
  entrega() {
    this.compraElem.nativeElement.classList.remove('optionActive');
    this.entregaElem.nativeElement.classList.add('optionActive');
    this.pagoElem.nativeElement.classList.remove('optionActive');
    this.compraState.active = false;
    this.compraState.up = true;
    this.entregaState.active = true;
    this.entregaState.up = false;
    this.entregaState.down = false;
    this.pagoState.active = false;
    this.pagoState.up = true;
  }
  pago() {
    this.compraElem.nativeElement.classList.remove('optionActive');
    this.entregaElem.nativeElement.classList.remove('optionActive');
    this.pagoElem.nativeElement.classList.add('optionActive');
    this.compraState.active = false;
    this.compraState.up = true;
    this.entregaState.active = false;
    this.entregaState.up = true;
    this.entregaState.down = false;
    this.pagoState.active = true;
    this.pagoState.up = false;
  }
  goTo(place: string) {
    this._sHome.goTo(place);
    if (place == 'Compra') {
      this.compra();
    } else if (place == 'Entrega') {
      this.entrega();
    } else if (place == 'Pago') {
      this.pago();
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalHomeComponent } from './total-home.component';

describe('TotalHomeComponent', () => {
  let component: TotalHomeComponent;
  let fixture: ComponentFixture<TotalHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

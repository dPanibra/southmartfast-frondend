import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from '@models/persona.model';
import { Pedido } from '@models/pedido.model';
import { PersonaService } from '@services/persona.service';
import { PedidoService } from '@services/pedido.service';
import { ProductoPedidoService } from '@services/productoPedido.service';
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl,
} from '@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { ScrollService } from '@services/scroll.service';
import { HomeService } from '@services/home.service';
import { ConfigService } from '@services/config.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-formulario-home',
  templateUrl: './formulario-home.component.html',
  styleUrls: ['./formulario-home.component.scss'],
})
export class FormularioHomeComponent implements OnInit {
  @ViewChild('formHome') formHome: ElementRef;
  @ViewChild('formMetodo') formMetodo: ElementRef;
  distritoControl = new FormControl('', Validators.required);
  selectFormControl = new FormControl('', Validators.required);
  distritos: any[] = [
    { name: 'Alto Selva Alegre', precio: '5.00' },
    { name: 'Cercado Arequipa', precio: '5.00' },
    { name: 'Cayma', precio: '5.00' },
    { name: 'Cerro Colorado', precio: '5.00' },
    { name: 'Jacobo Hunter', precio: '5.00' },
    { name: 'José Luis Bustamante y Rivero', precio: '5.00' },
    { name: 'Mariano Melgar', precio: '5.00' },
    { name: 'Miraflores', precio: '5.00' },
    { name: 'Paucarpata', precio: '5.00' },
    { name: 'Sachaca', precio: '5.00' },
    { name: 'Socabaya', precio: '5.00' },
    { name: 'Tiabaya', precio: '5.00' },
    { name: 'Yanahuara', precio: '5.00' },
  ];
  myForm: FormGroup;
  newPerson = new Persona(
    0,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    -16.39889,
    -71.535,
    ''
  );
  btnState = {
    normal: true,
    disableBtn: false,
    loader: false,
    success: false,
  };
  selected = new FormControl(0);
  observacion: string = '';
  metodosPago = [
    'Yape',
    'Efectivo',
    // 'Depósito',
    'Tarjeta de Crédito/Débito',
  ];
  totalPedido: number = 0;

  whatsApp = {
    number: '',
    load: false,
    link: '',
  };
  suscribeConfig: Subscription;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private _sPersona: PersonaService,
    private _sPedido: PedidoService,
    private _sProductoPedidoService: ProductoPedidoService,
    private _snackBar: MatSnackBar,
    private _sScroll: ScrollService,
    private _sHome: HomeService,
    private _sConfig: ConfigService
  ) {
    this.myForm = this.formBuilder.group({
      nombre: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          this.noWhitespaceValidator,
        ],
      ],
      email: ['', [Validators.required, Validators.email]],
      celular: ['', [Validators.required, this.minNumbersValidator]],
      direccion: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          this.noWhitespaceValidator,
        ],
      ],
      referencia: ['', [this.noWhitespaceValidator]],
      observacion: ['', [this.noWhitespaceValidator]],
    });
  }

  ngOnInit(): void {
    window.onscroll = () => {
      this._sScroll.entregaTop = this.formHome.nativeElement.offsetTop;
      this._sScroll.pagoTop = this.formMetodo.nativeElement.offsetTop;
    };
    this._sHome.change.subscribe((place) => {
      if (place == 'Compra') {
        window.scrollTo(0, 300);
      } else if (place == 'Entrega') {
        window.scrollTo(0, this._sScroll.entregaTop + 10);
      } else if (place == 'Pago') {
        window.scrollTo(0, this._sScroll.pagoTop + 10);
      }
    });
    this.suscribeConfig=this._sConfig.getConfig().subscribe((generalConfig) => {
      let nombreEmpresa = generalConfig.nombreEmpresa.split(' ').join('%20');
      this.whatsApp.load = true;
      this.whatsApp.number = generalConfig.celPagos;
      this.whatsApp.link = `https://api.whatsapp.com/send?phone=51${generalConfig.celPagos}&text=Estimados%20${nombreEmpresa}%20envio%20mi%20Voucher`;
    });
  }
  ngOnDestroy(): void {
    this.suscribeConfig.unsubscribe();
  }

  onMapReady(map) {
    map.addListener('click', (e) => {
      this.newPerson.lat = e.latLng.lat();
      this.newPerson.lon = e.latLng.lng();
    });
  }

  onSubmit() {
    let number = this._sProductoPedidoService.productosCard.length;
    if (number == 0) {
      this.openSnackBar();
      window.scrollTo(0, 300);
    } else {
      if (this.myForm.valid === true) {
        this.myForm.disable();
        this.btnState.disableBtn = true;
        this.btnState.normal = false;
        this.btnState.loader = true;

        this.llenarDatos();
      } else if (this.myForm.valid === false) {
        let top = this.formHome.nativeElement.offsetTop;
        window.scrollTo(0, top);
      }
    }
  }
  llenarDatos() {
    this._sPedido.getTotalPedido().subscribe((totalPedido) => {
      this.totalPedido = +totalPedido;
    });

    let pedidoActual = new Pedido(
      0,
      '',
      '',
      this.observacion,
      this.metodosPago[this.selected.value],
      this.totalPedido,
      0,
      'Pendiente',
      '',
      this.newPerson.nombre,
      this.newPerson.email,
      this.newPerson.telefono,
      this.newPerson.direccion,
      this.newPerson.referencia,
      this.newPerson.lat,
      this.newPerson.lon,
      this.distritoControl.value.name,
      0
    );
    this._sPedido.pedidoActual = pedidoActual;

    this._sPersona.personValidator = true;
    this._sPersona.newPerson = this.newPerson;
    this._sPersona.newPerson.distrito = this.distritoControl.value.name;
    // console.log(this._sPersona.newPerson);
    // console.log(this._sPedidoService.pedidoActual);

    setTimeout(() => {
      this.goToConfirm();
    }, 800);
  }
  goToConfirm() {
    this.btnState.loader = false;
    this.btnState.success = true;
    setTimeout(() => {
      this.router.navigateByUrl('/confirmarPedido');
    }, 500);
  }

  selectDistrito(data) {
    this._sPedido.updateDelivery(data);
  }

  openSnackBar() {
    let config = new MatSnackBarConfig();
    config.duration = 3000;
    config.panelClass = ['error-snackbar'];
    config.horizontalPosition = 'center';
    config.verticalPosition = 'top';

    this._snackBar.open('Debe agregar uno o más Productos', 'x', config);
  }
  noWhitespaceValidator(control: FormControl) {
    if (control.value.toString().length == 0) {
      const isValid = true;
      return isValid;
    } else {
      const isWhitespace = (control.value || '').trim().length == 0;
      const isValid = !isWhitespace;
      return isValid ? null : { whitespace: true };
    }
  }
  minNumbersValidator(control: FormControl) {
    if (control.value.toString().length == 0) {
      const isValid = true;
      return isValid;
    } else {
      const minNumber = control.value.toString().length == 9;
      const isValid = minNumber;
      return isValid ? null : { minnumber: true };
    }
  }
}

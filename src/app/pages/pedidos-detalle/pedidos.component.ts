import { Component, OnInit, Renderer2, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {MatDatepicker} from '@angular/material/datepicker';
// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import { Moment} from 'moment';

const moment =  _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss'],
  // providers: [
  //   {
  //     provide: DateAdapter,
  //     useClass: MomentDateAdapter,
  //     deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
  //   },

  //   {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  // ],
})
export class PedidosComponent implements OnInit {
  
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });
  date = new FormControl(moment([2017, 0, 1]));


  date2 = new FormControl(moment());

  // chosenYearHandler(normalizedYear: Moment) {
  //   const ctrlValue = this.date2.value;
  //   ctrlValue.year(normalizedYear.year());
  //   this.date2.setValue(ctrlValue);
  // }

  // chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
  //   const ctrlValue = this.date2.value;
  //   ctrlValue.month(normalizedMonth.month());
  //   this.date2.setValue(ctrlValue);
  //   datepicker.close();
  // }
  constructor(private _renderer2: Renderer2, @Inject(DOCUMENT) private _document: Document) {}

  ngOnInit(): void {  
    //jQuery
    var jQueryCdn = this._renderer2.createElement('script');
    jQueryCdn.onload = this.loadCustomJS.bind(this);
    jQueryCdn.type = 'text/javascript';
    jQueryCdn.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js';
    this._renderer2.appendChild(this._document.body, jQueryCdn);
  }

  loadCustomJS(){
    //Append Custom Script
    var script = this._renderer2.createElement('script');
    script.type = 'text/javascript';
    script.src = "http://api.store.southmartfast.com/js/custom-code-pedido-especifico.js"; //Modificar aqui el path del js "custom-code-pedido-especifico.js"
    this._renderer2.appendChild(this._document.body, script);
  }

}

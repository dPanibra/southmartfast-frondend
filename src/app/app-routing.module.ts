import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogoutGuard } from '@guards/logout.guard';
import { LogedGuard } from '@guards/loged.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'confirmarPedido',
    loadChildren: () =>
      import('./pages/confirmar-pedido/confirmar-pedido.module').then(
        (m) => m.ConfirmarPedidoModule
      ),
  },
  {
    path: 'finalizarPedido',
    loadChildren: () =>
      import('./pages/finalizar-pedido/finalizar-pedido.module').then(
        (m) => m.FinalizarPedidoModule
      ),
  },
  {
    path: 'pedidos',
    canActivate:[LogedGuard],
    loadChildren: () =>
      import('./pages/pedidos/pedidos.module').then((m) => m.PedidosModule),
  },
  {
    path: 'pedidos/:id',
    canActivate:[LogedGuard],
    loadChildren: () =>
      import('./pages/pedidos-detalle/pedidos.module').then((m) => m.PedidosModule),
  },
  {
    path: 'login',
    canActivate:[LogoutGuard],
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginModule),
  },
  {
    path: 'productos',
    canActivate:[LogedGuard],
    loadChildren: () =>
      import('./pages/productos/productos.module').then((m) => m.ProductosModule),
  },
  {
    path: 'categorias',
    canActivate:[LogedGuard],
    loadChildren: () =>
      import('./pages/categorias/categorias.module').then((m) => m.CategoriasModule),
  },
  {
    path: 'unidadesdemedida',
    canActivate:[LogedGuard],
    loadChildren: () =>
      import('./pages/unidades/unidades.module').then((m) => m.UnidadesModule),
  },
  {
    path: 'general',
    canActivate:[LogedGuard],
    loadChildren: () =>
      import('./pages/general/general.module').then((m) => m.GeneralModule),
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { LoginService } from '@services/login.service';
import { ConfigService } from '@services/config.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  isLoged: boolean = false;

  constructor(private _sLogin: LoginService, private _Config: ConfigService) {}

  ngOnInit(): void {
    this._sLogin.getisLoged().subscribe((isLoged) => {
      this.isLoged = isLoged;
    });
    this._Config.getConfig().subscribe((data) => {
      // console.log('',data);
    });
  }
}
